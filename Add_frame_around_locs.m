function [locs_including_frame] = Add_frame_around_locs(locs_wo_frame, frame_size, locsColumns)

number_of_locs_wo_frame = numel(locs_wo_frame(:,1));

locs_including_frame = locs_wo_frame;


% % Adds localisation at the 0,0 coordinate
% locs_including_frame(number_of_locs_wo_frame+1,:) = 0;  


 
locs_including_frame((number_of_locs_wo_frame+1):(number_of_locs_wo_frame+4*frame_size),:) = 1; % initializes all locs for the frame with 1

% Adds localisations at x-coordinate = 1; y-coordinate = 1 ... frame size
for i = 1:frame_size
    locs_including_frame(number_of_locs_wo_frame+i, locsColumns.yCol) = i; 
end

% % Adds localisation at the max,1 coordinate 
% locs_including_frame((number_of_locs_wo_frame+frame_size+1):((number_of_locs_wo_frame+frame_size)*2),:) = 1; 

% Adds localisations at x-coordinate = 1 ... frame size; y-coordinate = 1
for i = 1:frame_size
    locs_including_frame(number_of_locs_wo_frame+frame_size+i, locsColumns.xCol) = i;
end


% Adds localisations at x-coordinate = frame size; y-coordinate = 1 ... frame size
locs_including_frame(number_of_locs_wo_frame+2*frame_size+1:number_of_locs_wo_frame+3*frame_size, locsColumns.xCol) = frame_size;
for i = 1:frame_size
    locs_including_frame(number_of_locs_wo_frame+2*frame_size+i, locsColumns.yCol) = i;
end

% Adds localisations at x-coordinate = 1 ... frame size; y-coordinate = frame size
locs_including_frame(number_of_locs_wo_frame+3*frame_size+1:number_of_locs_wo_frame+4*frame_size, locsColumns.yCol) = frame_size;
for i = 1:frame_size
    locs_including_frame(number_of_locs_wo_frame+3*frame_size+i, locsColumns.xCol) = i;
end


%locs_including_frame(number_of_locs_wo_frame+2, locsColumns.xCol) = frame_size;
