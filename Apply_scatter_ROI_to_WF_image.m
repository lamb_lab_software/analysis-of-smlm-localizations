function [size_WF_image, varargout] = Apply_scatter_ROI_to_WF_image(WF_path, WF_name, savepath_segmented_WF_image, savename_segmented_WF_image, save_name_mask, segmentation_type, pixel_size_WF_image, varargin)

addpath('X:\Data\Inflammasome_Ivo_2015\STORM software\RCC drift correction DBSCAN clustering Ivo');

WF_image = imread(strcat(WF_path,WF_name));
% figure; imshow(WF_image, [150 400]);

size_WF_image = length(WF_image);

cd(savepath_segmented_WF_image);

switch nargin
    
    case 8        
            
            if isequal(strcmp(segmentation_type, "entire cell"),1)          % entire cell segmentation
            
                scatter_ROI_position_entire_cell = varargin{1};             

                % Get coordinates of mask for entire cell 
                entire_cell_mask_position(:,1) = (scatter_ROI_position_entire_cell(:,1)/pixel_size_WF_image)+3;
                entire_cell_mask_position(:,2) = (scatter_ROI_position_entire_cell(:,2)/pixel_size_WF_image)+3;
                %figure; scatter(scatter_ROI_position_entire_cell(:,1), scatter_ROI_position_entire_cell(:,2));

                % Create mask for entire cell 
                entire_cell_mask = poly2mask(entire_cell_mask_position(:,1), entire_cell_mask_position(:,2), size_WF_image, size_WF_image);        
                %figure; imshow(entire_cell_mask);  
                
                % Enlarges the mask by 3 pixels
                entire_cell_mask_enlarged = imdilate(entire_cell_mask, ones(3,3));
                i=1;
                while i<3
                    entire_cell_mask_enlarged = imdilate(entire_cell_mask_enlarged, ones(3,3));
                    i=i+1;
                end

                entire_cell_mask_enlarged = uint16(entire_cell_mask_enlarged);               

                % Save mask for entire cell 
                save(save_name_mask,'entire_cell_mask_enlarged');

                % Segment entire cell
                segmented_WF_entire_cell = WF_image.*entire_cell_mask_enlarged;
                % figure; imshow(segmented_WF_entire_cell, [150 400]);
                varargout{1} = segmented_WF_entire_cell;
                
                % Save WF image with entire cell segmented
                imwrite(segmented_WF_entire_cell, strcat (savename_segmented_WF_image));
                
                % Calculate integrated intensity of speck (manual)  
                integrated_intensity_WF_entire_cell = sum(segmented_WF_entire_cell,'all');
                varargout{2} = integrated_intensity_WF_entire_cell;
                
                % Calculate area in um^2 of entire cell
                area_entire_cell_um2 = ((nnz(segmented_WF_entire_cell)*(pixel_size_WF_image^2))/1000000);
                varargout{3} = area_entire_cell_um2;
            
            end  
            
            
            
            

            if isequal(strcmp(segmentation_type, "speck"),1)                % Speck segmentation               
        
                scatter_ROI_position_manual_speck = varargin{1};             

                % Get coordinates of manual speck mask 
                manual_speck_mask_position(:,1) = (scatter_ROI_position_manual_speck(:,1)/pixel_size_WF_image)+3;
                manual_speck_mask_position(:,2) = (scatter_ROI_position_manual_speck(:,2)/pixel_size_WF_image)+3;
                %figure; scatter(scatter_ROI_position_manual_speck(:,1), scatter_ROI_position_manual_speck(:,2));

                % Create manual speck mask 
                manual_speck_mask = poly2mask(manual_speck_mask_position(:,1), manual_speck_mask_position(:,2), size_WF_image, size_WF_image);        
                %figure; imshow(manual_speck_mask);  
                
                % Enlarges the mask by 3 pixels
                manual_speck_mask_enlarged = imdilate(manual_speck_mask, ones(3,3));
                i=1;
                while i<3
                    manual_speck_mask_enlarged = imdilate(manual_speck_mask_enlarged, ones(3,3));
                    i=i+1;
                end

                manual_speck_mask_enlarged = uint16(manual_speck_mask_enlarged);               

                % Save speck mask (manual ROI) 
                save(strcat(save_name_mask, '_manual.mat'),'manual_speck_mask_enlarged');

                % Segment speck (manual)
                manual_segmented_WF_speck = WF_image.*manual_speck_mask_enlarged;
                % figure; imshow(manual_segmented_WF_speck, [150 400]);
                varargout{1} = manual_segmented_WF_speck;
                
                % Save WF image with manually segmented speck 
                imwrite(manual_segmented_WF_speck, strcat (savename_segmented_WF_image, '_manual.tiff'));
                
                % Calculate integrated intensity of speck (manual)  
                integrated_intensity_WF_manual_speck = sum(manual_segmented_WF_speck,'all');
                varargout{2} = integrated_intensity_WF_manual_speck;

                % Calculate area in um^2 of speck (manual)
                area_speck_manual_um2 = ((nnz(manual_segmented_WF_speck)*(pixel_size_WF_image^2))/1000000);
                varargout{3} = area_speck_manual_um2;
                
                
                               
                % Create circular ROI selection
                figure;
                %original: imshow(WF_image, [150 400]);
                imshow(WF_image, [200 700]);
                title('Please select speck, hit "c" key to continue');
                Speck_WF_ROI = drawcircle;
                pause;

                Circular_speck_mask_temp = createMask(Speck_WF_ROI);

                close;
                clear Speck_WF_ROI

                Circular_speck_mask_temp = uint16(Circular_speck_mask_temp); 

                Speck_find_center_image = WF_image.*Circular_speck_mask_temp;
                %figure; imshow(Speck_find_center_image, [150 400]);    

                % Find coordinates of center of mass of speck
                binaryImage             = true(size(Speck_find_center_image));
                labeledImage            = bwlabel(binaryImage);
                measurements            = regionprops(labeledImage, Speck_find_center_image, 'WeightedCentroid');
                centerOfMass            = measurements.WeightedCentroid;
                centerOfMass_rounded    = round(centerOfMass);
                % Speck_find_center_image(centerOfMass_rounded(1,2),centerOfMass_rounded(1,1))=0;
                % figure; imshow(Speck_find_center_image, [150 400]);

                % Code for squared speck ROI 
                % x_min = centerOfMass_rounded(1,1)-17;
                % x_max = centerOfMass_rounded(1,1)+17;
                % y_min = centerOfMass_rounded(1,2)-17;
                % y_max = centerOfMass_rounded(1,2)+17;        

                % Segment speck
                % segmented_WF_area = WF_image(y_min:y_max,x_min:x_max);

                % Create and save speck mask 
                % speck_mask = zeros(size_WF_image,size_WF_image);

                % speck_mask(y_min:y_max,x_min:x_max) = 1;


                % Initialize parameters for circular speck mask
                speck_circleCenterX   = centerOfMass_rounded(1,1); 
                speck_circleCenterY   = centerOfMass_rounded(1,2); 
                speck_circleRadius    = 17;    

                % Create circular speck mask 
                circular_speck_mask = false(size_WF_image,size_WF_image); 
                [x, y] = meshgrid(1:size_WF_image, 1:size_WF_image); 
                circular_speck_mask((x - speck_circleCenterX).^2 + (y - speck_circleCenterY).^2 <= speck_circleRadius.^2) = true; 
                % figure; imshow(circular_speck_mask, []); 

                circular_speck_mask = uint16(circular_speck_mask);

                save(strcat(save_name_mask, '_circular.mat'),'circular_speck_mask'); 
                
                % Segment speck (circular)
                circular_segmented_WF_speck = WF_image.*circular_speck_mask;
                % figure; imshow(circular_segmented_WF_speck, [150 400]);
                varargout{4} = circular_segmented_WF_speck;
                
                % Save WF image with circular segmented speck
                imwrite(circular_segmented_WF_speck, strcat (savename_segmented_WF_image, '_circular.tiff'));
                
                % Calculate integrated intensity of entire cell  
                integrated_intensity_WF_circular_speck = sum(circular_segmented_WF_speck,'all');
                varargout{5} = integrated_intensity_WF_circular_speck;
                
                % Calculate area in um^2 of speck (manual) 
                area_speck_circular_um2 = ((nnz(circular_segmented_WF_speck)*(pixel_size_WF_image^2))/1000000);
                varargout{6} = area_speck_circular_um2;
            
            end
        
    case 10 % Cytoplasma segmentation
        
            name_entire_cell_mask    = varargin{1};

            name_manual_speck_mask   = varargin{2};
            
            name_circular_speck_mask = varargin{3};

            % Load mask of entire cell
            load(name_entire_cell_mask); 

            % Segment entire cell
            segmented_WF_area_entire_cell = WF_image.*entire_cell_mask_enlarged;

            
            %-------- manual speck            
            
            % Load manual speck mask
            load(name_manual_speck_mask);

            % Invert manual speck mask
            manual_speck_mask_enlarged = ~manual_speck_mask_enlarged;

            manual_speck_mask_enlarged = uint16(manual_speck_mask_enlarged);            

            % Segment cytoplasma (manual speck mask)
            manual_segmented_WF_cytoplasma = segmented_WF_area_entire_cell.*manual_speck_mask_enlarged;
            % figure; imshow(manual_segmented_WF_cytoplasma, [150 400]);
            varargout{1} = manual_segmented_WF_cytoplasma;
            
            % Save WF image cytoplasma (manually segmented speck)
            imwrite(manual_segmented_WF_cytoplasma, strcat (savename_segmented_WF_image, '_manual_speck.tiff'));
            
            % Calculate integrated intensity of cytoplasma (manual speck) 
            integrated_intensity_WF_cytoplasma_manual = sum(manual_segmented_WF_cytoplasma,'all');
            varargout{2} = integrated_intensity_WF_cytoplasma_manual;

            % Calculate area in um^2 of cytoplasma (manual speck) 
            area_cytoplasma_manual_um2 = ((nnz(manual_segmented_WF_cytoplasma)*(pixel_size_WF_image^2))/1000000);
            varargout{3} = area_cytoplasma_manual_um2;
            
            
            %---- circular speck            
            
            % Load circular speck mask
            load(name_circular_speck_mask);

            % Invert circular speck mask
            circular_speck_mask = ~circular_speck_mask;

            circular_speck_mask = uint16(circular_speck_mask);            

            % Segment cytoplasma (circular speck mask)
            circular_segmented_WF_cytoplasma = segmented_WF_area_entire_cell.*circular_speck_mask;
            % figure; imshow(circular_segmented_WF_cytoplasma, [150 400]);
            varargout{4} = circular_segmented_WF_cytoplasma;
            
            % Save WF image cytoplasma (circular segmented speck)
            imwrite(circular_segmented_WF_cytoplasma, strcat (savename_segmented_WF_image, '_circular_speck.tiff'));
            
            % Calculate integrated intensity of cytoplasma (circular speck) 
            integrated_intensity_WF_cytoplasma_circular = sum(circular_segmented_WF_cytoplasma,'all');
            varargout{5} = integrated_intensity_WF_cytoplasma_circular;
            
            % Calculate area in um^2 of cytoplasma (circular speck) 
            area_cytoplasma_circular_um2 = ((nnz(circular_segmented_WF_cytoplasma)*(pixel_size_WF_image^2))/1000000);
            varargout{6} = area_cytoplasma_circular_um2;
      
end     


               

