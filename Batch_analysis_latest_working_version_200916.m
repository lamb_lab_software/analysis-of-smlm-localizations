clear, clc, close all

addpath('X:\Data\Inflammasome_Ivo_2015\STORM software\RCC drift correction DBSCAN clustering Ivo');
%% Ask whether WF images should be included in the analysis

WF_answer = questdlg(['Would you like to include widefield snapshots in the analysis?' newline], 'Widefield analysis', 'Yes', 'No', 'Yes');

switch WF_answer
    
    case 'Yes'
        analysis_type = 'Incl_WF';
        WF_path       = 'X:\Data\Inflammasome_Ivo_2015\STORM_experiments\dSTORM_Lausanne\Localisation_data\3rd_data_set_11-11-2019_Inflammasome\2019-11-06_CS_Inflammasome\WF data';
        
    case 'No'
        analysis_type = 'Excl_WF';
        
        %% Asks for the size of the input image as it is needed as an input for the RCC drift correction
        prompt      = {'Please enter the image size (in pixels^2)'};
        dlgtitle    = 'Size input image';
        definput    = {'639'};
        image_size  = inputdlg(prompt,dlgtitle,[1 45],definput);
        image_size  = str2double(image_size);

end

%% Specifies the pixel size of the raw movie data
pixel_size  = 106; % nm

%% Specifies the localizer used for the files to be loaded

localizer_answer = questdlg(['Which localizer was used to generate the files to be loaded?' newline], 'Used localizer', 'Fang localizer', 'The other one', 'Fang localizer');
    
   switch localizer_answer
           
       case 'Fang localizer'
            Localizer = "Fang";       

       case 'The other one'      
            Localizer = "The other one";

       case ''
            fprintf('\n -- Analysis aborted -- \n')
            return   
       
   end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%START%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Selection of files for batch analysis%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
   
% Specifies the path to the folder in which the raw localisation files are saved
%%

defaultPath = 'X:\Data\Inflammasome_Ivo_2015\STORM_experiments\dSTORM_Lausanne\Localisation_data\3rd_data_set_11-11-2019_Inflammasome\2019-11-06_CS_Inflammasome\locResults\Raw locs Lausanne';    

batch_database = struct;
        
cancel = false;
    
FOV_counter = 1;
    
while ~cancel       % This keeps on adding raw localisation files to the batch analysis until the "Cancel" button is pressed

    %% Gets paths to raw localisation file(s)               

    [Localisation_file_name, Localisation_file_path] = uigetfile('.csv',...
    'Select file containing unfiltered localisations',...
    'MultiSelect', 'on',...
    defaultPath);

    if isequal(Localisation_file_name,0)    % If the cancel button is hit the while loop is left
       cancel = true;
       continue
    end        

    File.Locs_name = Localisation_file_name;     % Saves the name and the path of the currently selected file
    File.Locs_path = Localisation_file_path;
    
    disp(['Localisation file added:', ' ', File.Locs_name])
    
    %% Gets paths to widefield file(s)
    switch analysis_type
        
        case 'Incl_WF'                       

        [WF_file_name, WF_file_path] = uigetfile('.tif',...
        'Select widefield image corresponding to localisation file selected last',...
        'MultiSelect', 'on',...
        WF_path);          

        File.WF_name = WF_file_name;     % Saves the name and the path of the currently selected file
        File.WF_path = WF_file_path;  
        
        disp(['Widefield file added:', ' ', File.WF_name])
        
        
        % Check whether selected localisation and widefield file correspond
        corrected_WF_file = 0;    
        if isequal(strcmp (File.Locs_name(1,14:15),File.WF_name(1,16:17)),0)
            Locs_and_WF_same = questdlg(['Loaded localisation file and widefield snapshot do not seem to correspond!' newline newline 'Loaded localisation file: ' File.Locs_name newline newline 'Loaded wiedfield file: ' File.WF_name newline newline 'Would you like to proceed with the analysis or would you like to correct the last file addition step?' newline], 'Localisations and widefield do not correspond', 'Proceed analysis', 'Correct last addition step', 'Correct last addition step');

            if isequal(strcmp(Locs_and_WF_same, 'Correct last addition step'),1)           
               corrected_WF_file = 1; 
            else
               disp('Analysis proceeded')         
            end 
        end

        if corrected_WF_file == 1
            disp('File loading corrected')
            continue
        end
        
        %% Determine size of image from loaded WF image
    
        WF_image   = imread(strcat(File.WF_path,File.WF_name));
        % figure; imshow(WF_image,[150 400]);
        image_size = length(WF_image);
        frame_size = image_size*pixel_size;
        clear WF_image
        
    end     
    
    %% Creates saving directory for current FOV
        raw_path    = File.Locs_path;
        path        = raw_path(1:end-1);                                        % Removes the "\" at the end of the raw path; not sure if this is really necessary 
        raw_name    = File.Locs_name;
        
        % Crops the raw data sample name
        find_sample_name = strfind(raw_name,'_1_');
        if numel(find_sample_name) == 3
            sample_name = raw_name(1,1:find_sample_name(1,2)+1);
        else
            sample_name = raw_name(1,1:find_sample_name(1,1)+1);
        end            
        
        % Creates saving directory
        savepath    = strcat(path, '\PerCell_', sample_name);          
        mkdir (savepath);                                                   % Creates the saving directory   
    
    
    %% Load selected file
    
    cd(File.Locs_path(1:end-1));                 % Switches the current path into the folder in which the file added last is saved
    
    locs     = dlmread(File.Locs_name,',',1,0);  % Reads in the localisations from the file added last (with a row offset of 1 as it contains the text specifying the columns)
    
    fprintf('\n -- Loading localisation coordinates ... \n')

    file          = fopen(File.Locs_name);           % Connects the file object to the instrument
    line          = fgetl(file);                % Gets the text from the first line (text sepecifications what's in the columns) seperated by commas 
    header        = regexp(line, ',', 'split'); % Puts the text from every column into a seperate column 
    
    switch Localizer
        
        case "Fang" 
            locsColumns.xCol            = strmatch('x [nm]',header);        % Finds the column matching "x [nm]"
            locsColumns.yCol            = strmatch('y [nm]',header);
            %locsColumns.zCol           = strmatch('z [nm]',header);
            locsColumns.frameCol        = strmatch('frame',header);
            locsColumns.intensityCol    = strmatch('intensity [photon]',header);
            locsColumns.uncertCol       = strmatch('uncertainty [nm]',header);      
            locsColumns.offsetCol       = strmatch('offset [photon]',header);
            locsColumns.LLCol           = strmatch('loglikelihood',header);
            locsColumns.sigmaCol        = strmatch('sigma [nm]',header);
            locsColumns.xCol_px         = 9;                                % these columns don't exist for the "Fang" localizer but are generated after drift correction in this script and saved in the "locs" variable
            locsColumns.yCol_px         = 10;
            
        case "The other one"
            % Saves the columns in which the indicated localisation properties are saved
            locsColumns.xCol            = strmatch('x_nm',header);                              
            locsColumns.yCol            = strmatch('y_nm',header);
            locsColumns.zCol            = strmatch('z_nm',header);
            locsColumns.frameCol        = strmatch('frame',header);
            locsColumns.LLCol           = strmatch('logLikelyhood',header);
            locsColumns.photonsCol      = strmatch('photons',header);
            locsColumns.xCol_px         = strmatch('x_pix',header);
            locsColumns.yCol_px         = strmatch('y_pix',header);
            locsColumns.bgCol           = strmatch('crlb_background',header);
            
    end
    
    fprintf('\n -- Data Loaded -- \n')
    
    %% RCC drift-correct localisations 
    
    fitting_dist = 'X:\Data\Inflammasome_Ivo_2015\STORM software\STORM software Christian\scripts\spline_fitting_distribution'; 
    RCC_path = strcat(fitting_dist,'\RCC_drift_correct');
    
    %original: [locs_DC] = RCC_drift_correct_locs(locs, locsColumns, "2D", "Fang", pixel_size, image_size, savepath, strcat(File.Locs_name(1:end-4),'_DC'), RCC_path);
    [locs_DC] = RCC_drift_correct_locs(locs, locsColumns, "2D", Localizer, pixel_size, image_size, savepath, strcat(sample_name,'_locs_DC'), RCC_path);
    
    % Add frame around locs
    locs_DC_framed = Add_frame_around_locs(locs_DC, frame_size, locsColumns);
    
    % Writes the unfiltered drift-corrected, framed locs to a ThunderStorm file
    Write_Thunderstorm_locs(locs_DC_framed, locsColumns, '2D', Localizer, ['FOV_', num2str(FOV_counter), ' ', 'unfilt_locs_TS.csv'], savepath);    
    
    clear locs
    
    %% Plot coordinates + select cell outlines
    
    % Plots the localisations of the file selected last
    figure; 
    localisation_plot = scatter(locs_DC(:,locsColumns.xCol),locs_DC(:,locsColumns.yCol),2); 
    title('Please select Cell ROI(s), right-click to finish');
    xlabel('x [nm]');
    ylabel('y [nm]');
    hold on;
    
    Cells = struct;
    
    cancel_cell_ROI_selection = false;
    
    Cell_ROIs_counter = 1;       
    
    while ~cancel_cell_ROI_selection    % This keeps on adding free hand cell selection ROIs until the scatter plot is clicked with the right mouse button  
        
        Cell_ROI    = drawfreehand('LineWidth',2,'Color','r');              % Creates the ROI selection tool  
        click_type  = get(gcf,'SelectionType');                             % Checks whether plot was clicked with the left or right mouse button 
               
        if strcmp(click_type,'normal') % left click
            
            % Check whether last selection of locs needs to be corrected
            corrected_cell_locs = 0;    
            Last_cell_selection = questdlg('Would you like to correct the last selection of the cell outline?', 'Last selection of cell outline', 'Yes', 'No', 'No');

            if isequal(strcmp(Last_cell_selection, 'Yes'),1)           
               corrected_cell_locs = 1;
               disp('Cell outline selection corrected')
               delete(Cell_ROI)
               clear Cell_ROI
               continue                    
            end  
            
            Cell_completness = questdlg('Did the area selected last include an entire cell?', 'Cell completness', 'Yes', 'No', 'Yes');
            if isequal(strcmp(Cell_completness, 'Yes'),1)
               Cells.(strcat('Cell_', num2str(Cell_ROIs_counter))).Cell_complete = true;
            else
               Cells.(strcat('Cell_', num2str(Cell_ROIs_counter))).Cell_complete = false;
            end
            
            logicals_current_cell_ROI_locs = inROI(Cell_ROI,locs_DC(:,locsColumns.xCol), locs_DC(:,locsColumns.yCol));              % Checks which of the locs are within the selected ROI (info as logical (1 or 0) for every localisation)
            Current_cell_ROI_locs = locs_DC(logicals_current_cell_ROI_locs,:);                                                      % Contains only the localisations within the current ROI
            Cells.(strcat('Cell_', num2str(Cell_ROIs_counter))).locs.entire_cell.logicals = logicals_current_cell_ROI_locs;         % Creates a variable "Cells" in which the logicals specifiying which localisations belong to the current cell ROI are saved
            
            switch analysis_type
                
                case 'Incl_WF'                    
                    [image_size, WF_entire_cell, integrated_intensity_WF_entire_cell, area_WF_entire_cell_um2]  = Apply_scatter_ROI_to_WF_image(WF_file_path, WF_file_name, savepath, strcat('Cell_', num2str(Cell_ROIs_counter), '_WF.tiff'), strcat('Cell_', num2str(Cell_ROIs_counter),'_entire_cell_mask.mat'), "entire cell", pixel_size, Cell_ROI.Position);
                    Cells.(strcat('Cell_', num2str(Cell_ROIs_counter))).WF_data.entire_cell.image               = WF_entire_cell;                    
                    Cells.(strcat('Cell_', num2str(Cell_ROIs_counter))).WF_data.entire_cell.int_intensity       = integrated_intensity_WF_entire_cell;                    
                    Cells.(strcat('Cell_', num2str(Cell_ROIs_counter))).WF_data.entire_cell.cell_size           = area_WF_entire_cell_um2;
                    
            end
            clear Cell_ROI            
            
            answer = questdlg('Does the current cell contain a speck?','Current cell: With or without speck?'); % Opens a question dialog
            
            switch answer
            
                case 'Yes'  % if "Yes" was pressed
               
                   figure;  % Plots the localisations of the cell selected last
                   localisation_plot = scatter(Current_cell_ROI_locs(:,locsColumns.xCol),Current_cell_ROI_locs(:,locsColumns.yCol),2); 
                   title('Please select speck(s), right-click to finish');
                   xlabel('x (from FOV origin) [nm]');
                   ylabel('y (from FOV origin) [nm]');
                   hold on; 

                   cancel_speck_ROI_selection = false;

                   Speck_ROIs_counter = 1; 

                   while ~cancel_speck_ROI_selection    % This keeps on adding free hand speck selection ROIs until the scatter plot is clicked with the right mouse button  
                        
                       Speck_ROI    = drawfreehand('LineWidth',2,'Color','r');                 % Creates the ROI selection tool  
                       click_type   = get(gcf,'SelectionType');  
                       
                           if   strcmp(click_type,'normal') % left click
                               
                                % Check whether last selection of locs needs to be corrected
                                corrected_speck_locs = 0;    
                                Last_speck_selection = questdlg('Would you like to correct the last selection of the speck outline?', 'Last selection of speck outline', 'Yes', 'No', 'No');

                                if isequal(strcmp(Last_speck_selection, 'Yes'),1)           
                                   corrected_speck_locs = 1;
                                   disp('Speck outline selection corrected')
                                   delete(Speck_ROI)
                                   clear Speck_ROI
                                   continue                    
                               end  

                                  logicals_current_speck_ROI_locs = inROI(Speck_ROI,locs_DC(:,locsColumns.xCol), locs_DC(:,locsColumns.yCol));                                            % Checks which of the locs are within the selected ROI (info as logical (1 or 0) for every localisation)
                                  Current_speck_ROI_locs = locs_DC(logicals_current_speck_ROI_locs,:);                                                                                 % Contains on the localisations within the current ROI
                                  Cells.(strcat('Cell_', num2str(Cell_ROIs_counter))).locs.specks.(strcat('Speck_', num2str(Speck_ROIs_counter))).logicals = logicals_current_speck_ROI_locs; % Saves the logicals specifiying which localisations belong to the current speck ROI 
                                  
                                  switch analysis_type
                                        
                                      case 'Incl_WF'
                                            [image_size, WF_current_speck_manual, integrated_intensity_WF_current_speck_manual, area_WF_speck_manual_um2, WF_current_speck_circular, integrated_intensity_WF_current_speck_circular, area_WF_speck_circular_um2] = Apply_scatter_ROI_to_WF_image(WF_file_path, WF_file_name, savepath, strcat('Cell_', num2str(Cell_ROIs_counter), '_speck_WF'), strcat('Cell_', num2str(Cell_ROIs_counter),'_speck_mask'), "speck", pixel_size, Speck_ROI.Position);
                                            Cells.(strcat('Cell_', num2str(Cell_ROIs_counter))).WF_data.(strcat('Speck_', num2str(Speck_ROIs_counter))).image.manual                = WF_current_speck_manual;
                                            Cells.(strcat('Cell_', num2str(Cell_ROIs_counter))).WF_data.(strcat('Speck_', num2str(Speck_ROIs_counter))).int_intensity.manual        = integrated_intensity_WF_current_speck_manual;
                                            Cells.(strcat('Cell_', num2str(Cell_ROIs_counter))).WF_data.(strcat('Speck_', num2str(Speck_ROIs_counter))).size.manual                 = area_WF_speck_manual_um2;
                                            Cells.(strcat('Cell_', num2str(Cell_ROIs_counter))).WF_data.(strcat('Speck_', num2str(Speck_ROIs_counter))).image.circular              = WF_current_speck_circular;
                                            Cells.(strcat('Cell_', num2str(Cell_ROIs_counter))).WF_data.(strcat('Speck_', num2str(Speck_ROIs_counter))).int_intensity.circular      = integrated_intensity_WF_current_speck_circular;
                                            Cells.(strcat('Cell_', num2str(Cell_ROIs_counter))).WF_data.(strcat('Speck_', num2str(Speck_ROIs_counter))).size.circular               = area_WF_speck_circular_um2;
                                  end                                    
                                  
                                  logicals_current_cell_wo_specks_locs = xor(logicals_current_cell_ROI_locs,logicals_current_speck_ROI_locs);         % THIS WOULD NEED AJUSTMENT in case there cells with multiple specks; currently only for a single speck 
                                  Current_cell_wo_speck_ROI_locs = locs_DC(logicals_current_cell_wo_specks_locs,:);                                    % Contains the localisations of the current cell wo the localisation inside the speck
                                  Cells.(strcat('Cell_', num2str(Cell_ROIs_counter))).locs.cell_wo_specks.logicals = logicals_current_cell_wo_specks_locs;        % Saves the logicals specifiying which localisations belong to the current cell wo speck
                                  
                                  switch analysis_type
                                        
                                      case 'Incl_WF'
                                            [image_size, WF_current_cell_wo_manual_specks, integrated_intensity_WF_current_cell_wo_manual_specks, area_WF_cytoplasma_manual_um2, WF_current_cell_wo_circular_specks, integrated_intensity_WF_current_cell_wo_circular_specks, area_WF_cytoplasma_circular_um2] = Apply_scatter_ROI_to_WF_image(WF_file_path, WF_file_name, savepath, strcat('Cell_', num2str(Cell_ROIs_counter), '_wo_speck_WF'), strcat('Cell_', num2str(Cell_ROIs_counter),'_inverted_speck_mask.mat'), "cytoplasma", pixel_size, strcat('Cell_', num2str(Cell_ROIs_counter),'_entire_cell_mask.mat'), strcat('Cell_', num2str(Cell_ROIs_counter),'_speck_mask_manual.mat'), strcat('Cell_', num2str(Cell_ROIs_counter),'_speck_mask_circular.mat')); %% NOTE: the inverted speck mask variable is not used here 
                                            Cells.(strcat('Cell_', num2str(Cell_ROIs_counter))).WF_data.cell_wo_specks.image.manual             = WF_current_cell_wo_manual_specks;
                                            Cells.(strcat('Cell_', num2str(Cell_ROIs_counter))).WF_data.cell_wo_specks.int_intensity.manual     = integrated_intensity_WF_current_cell_wo_manual_specks;
                                            Cells.(strcat('Cell_', num2str(Cell_ROIs_counter))).WF_data.cell_wo_specks.size.manual              = area_WF_cytoplasma_manual_um2;
                                            Cells.(strcat('Cell_', num2str(Cell_ROIs_counter))).WF_data.cell_wo_specks.image.circular           = WF_current_cell_wo_circular_specks;
                                            Cells.(strcat('Cell_', num2str(Cell_ROIs_counter))).WF_data.cell_wo_specks.int_intensity.circular   = integrated_intensity_WF_current_cell_wo_circular_specks;
                                            Cells.(strcat('Cell_', num2str(Cell_ROIs_counter))).WF_data.cell_wo_specks.size.circular            = area_WF_cytoplasma_circular_um2;
                                            
                                  end
                                  clear Speck_ROI                                  
                                  
                                  %% Increment speck count
                                  Speck_ROIs_counter = Speck_ROIs_counter + 1;

                           elseif strcmp(click_type,'alt') %right click

                                  cancel_speck_ROI_selection = true;        % If the figure is right-clicked the while loop is left
                                  close                                 % closes current figure showing the current cell
                                  continue

                                    %   Could be useful if later on the option for multiple specks in a single cell is implemented
                                    %                       for current_speck = 1:numel(fieldnames(Cells.(strcat('Cell_', num2str(Cell_ROIs_counter))).locs.specks));
                                    %                           
                                    %                           Cells.(strcat('Cell_', num2str(Cell_ROIs_counter))).locs.cytoplasm = Cells.(strcat('Cell_', num2str(Cell_ROIs_counter))).locs.cytoplasm 
                                    %                           
                                    %                       end    

                           end
                           
                   end

                case 'No'           % If the user clicked "No"   
                    
                    Cell_ROIs_counter = Cell_ROIs_counter + 1;
                    continue        % The user can select the next cell within the current FOV
                 
                case ''             % If the user closed the selection dialogue by clicking the cross in the upper right hand corner
                    
                    Cell_ROIs_counter = Cell_ROIs_counter + 1;
                    continue        % The user can select the next cell within the current FOV
             end
            
            
               %% Increment Cell ROI count
               Cell_ROIs_counter = Cell_ROIs_counter + 1;
        
        elseif strcmp(click_type,'alt') %right click
            
               cancel_cell_ROI_selection = true;        % If the figure is right-clicked the while loop is left
               close                                    % closes current figure showing the current field of view
               fprintf('\n -- Selection of cell ROI(s) completed -- \n')
               continue
            
        end
               
    end
    
    %% Saves the data belonging to the current localisation file/FOV
    batch_database.(strcat('FOV_', num2str(FOV_counter))).Filenames         = File;
    batch_database.(strcat('FOV_', num2str(FOV_counter))).Cells             = Cells; 
    batch_database.(strcat('FOV_', num2str(FOV_counter))).Image_size__pxls2 = image_size;
   
    %% Increment FOV count 
    FOV_counter = FOV_counter + 1;
    
    %% Saves the batch database data
    cd(defaultPath);
    save('Per_cell_batch_database_selc_WFonly.mat','batch_database');
    
end 
    
   answer = questdlg(['Finished adding localisation files for batch cluster analysis.' newline newline 'Number of localisation files added: ' num2str(FOV_counter-1) newline newline 'Would you like to proceed with the analysis?' newline], 'Files for analysis added', 'Yes', 'No', 'Yes');
    
   switch answer
           
   case 'No'
       fprintf('\n -- Analysis aborted -- \n')
       return
   case ''
       fprintf('\n -- Analysis aborted -- \n')
       return
   case 'Yes'

        %% Saves the batch database data
%         cd(defaultPath);
%         save('Per_cell_batch_database.mat','batch_database');

    %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%END%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Selection of files for batch analysis%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

    %addpath('X:\Data\Inflammasome_Ivo_2015\STORM software\RCC drift correction DBSCAN clustering Ivo');

    fitting_dist = 'X:\Data\Inflammasome_Ivo_2015\STORM software\STORM software Christian\scripts\spline_fitting_distribution';     % define path to fitting distribution
    
    % Initialize table for measurement results
    sz          = [0 24];
    varTypes    = {'string','string','string','logical','double','double','double','double','double','double','logical','double','double','double','double','double','double','double','double','double','double','double','double','double'};
    varNames    = {'Filepath_Locs','Filename_Locs','Cell_number','Cell_complete','Size_entire_cell__um2','Number_of_locs_entire_cell','Density_of_locs_entire_cell__locs_per_um2','Cluster_density_entire_cell__clusters_per_um2','Average_cluster_size_entire_cell__nm2','Average_number_of_locs_per_cluster_entire_cell','Cell_contains_speck','Number_of_locs_within_speck','Speck_size_rg__nm','Speck_density__no_of_locs_per_um2','Number_of_locs_within_cytoplasma','Area_cytoplasma__um2','Density_of_locs_within_cytoplasma__locs_per_um2','Cluster_density_within_cytoplasma__clusters_per_um2','Average_cluster_size_within_cytoplasma__nm2','Average_number_of_locs_per_cluster_within_cytoplasma','Speck_size_rg__nm_per_size_entire_cell__um2','Speck_density__no_of_locs_per_um2_per_size_entire_cell__um2','Speck_size_rg__nm_per_Number_of_locs_entire_cell','Speck_density__no_of_locs_per_um2_per_Number_of_locs_entire_cel'};
    Results     = table('Size',sz,'VariableTypes',varTypes,'VariableNames',varNames);
    
    switch analysis_type
           case 'Incl_WF'
               sz = [0 36];
               add_varTypes_WF  = {'string','string','double','double','double','double','double','double','double','double','double','double'};
               add_varNames_WF  = {'Filepath_WF','Filename_WF','Cell_size_WF__um2','Speck_size_WF_manual__um2','Speck_size_WF_circular__um2','Cytoplasma_size_WF_manual__um2','Cytoplasma_size_WF_circular__um2','Int_intensity_WF_entire_cell__counts','Int_intensity_WF_speck_manual__counts','Int_intensity_WF_speck_circular__counts','Int_intensity_WF_cytoplasma_manual__counts','Int_intensity_WF_cytoplasma_circular__counts'};
               varTypes_incl_WF = cat(2, varTypes, add_varTypes_WF);
               varNames_incl_WF = cat(2, varNames, add_varNames_WF);
               Results          = table('Size',sz,'VariableTypes',varTypes_incl_WF,'VariableNames',varNames_incl_WF);
    end
    
    %% Loading the data

    for current_FOV = 1:numel(fieldnames(batch_database))                       % Iterates over the loaded field of views

        close all
        
        %try
            
        % Sets the path to the localisation file of the current field of view
        raw_path    = batch_database.(strcat('FOV_', num2str(current_FOV))).Filenames.Locs_path;
        path        = raw_path(1:end-1);                                        % Removes the "\" at the end of the raw path; not sure if this is really necessary 
        raw_name    = batch_database.(strcat('FOV_', num2str(current_FOV))).Filenames.Locs_name;
        
        % Crops the raw data sample name
        find_sample_name = strfind(raw_name,'_1_');
        if numel(find_sample_name) == 3
            sample_name = raw_name(1,1:find_sample_name(1,2)+1);
        else
            sample_name = raw_name(1,1:find_sample_name(1,1)+1);
        end            
        
        % Sets the path to the saving directory of the current field of view
        %savepath    = strcat(path, '\', sample_name);
        savepath    = strcat(path, '\PerCell_', sample_name);          
        %mkdir (savepath);                                                 % Creates the saving directory
        
        cd(savepath);
        %cd(path);                                                          % Changes into the directory in which the currently analysed localisation file is saved

        % Reads in the localizations of the current FOV
        % original: filename      = strcat(raw_name(1:end-4),'_DC');                   % This would not be necessary but it keeps the variable names in the read in the same
        filename      = strcat(sample_name,'_locs_DC');                   % This would not be necessary but it keeps the variable names in the read in the same
        locs_DC       = dlmread([filename '.csv'],',',1,0);                          

        file          = fopen([filename '.csv']);
        line          = fgetl(file);
        header        = regexp( line, ',', 'split' );
        
        switch Localizer 
            case "Fang"                 
                locsColumns.xCol            = strmatch('x [nm]',header);                % Finds the column matching "x [nm]"
                locsColumns.yCol            = strmatch('y [nm]',header);
                %locsColumns.zCol           = strmatch('z [nm]',header);
                locsColumns.frameCol        = strmatch('frame',header);
                locsColumns.intensityCol    = strmatch('intensity [photon]',header);
                locsColumns.uncertCol       = strmatch('uncertainty [nm]',header);      
                locsColumns.offsetCol       = strmatch('offset [photon]',header);
                locsColumns.LLCol           = strmatch('loglikelihood',header);
                locsColumns.sigmaCol        = strmatch('sigma [nm]',header);
                locsColumns.xCol_px         = strmatch('x [pixels]',header);            % these columns don't exist for the "Fang" localizer but are generated after drift correction in this script and saved in the "locs" variable
                locsColumns.yCol_px         = strmatch('y [pixels]',header);
           
            case "The other one"         
                % Saves the columns in which the indicated localisation properties are saved
                locsColumns.xCol            = strmatch('x_nm',header);                              
                locsColumns.yCol            = strmatch('y_nm',header);
                locsColumns.zCol            = strmatch('z_nm',header);
                locsColumns.frameCol        = strmatch('frame',header);
                locsColumns.LLCol           = strmatch('logLikelyhood',header);
                locsColumns.photonsCol      = strmatch('photons',header);
                locsColumns.xCol_px         = strmatch('x_pix',header);
                locsColumns.yCol_px         = strmatch('y_pix',header);
                locsColumns.bgCol           = strmatch('crlb_background',header);
        end
        
        %pixel_size          = 106; % nm
        SR_pixel_size       = (pixel_size/10);
        image_size          = batch_database.(strcat('FOV_', num2str(current_FOV))).Image_size__pxls2;
        frame_size          = image_size*pixel_size;
        
        display(['Currently file "' filename '" is analysed (File ' int2str(current_FOV) ' out of ' int2str(numel(fieldnames(batch_database))) ' total files.)']);


    %% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%START%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
       %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Drift Correction%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%         % Prepares data for RCC-based drift correction
% 
%         fprintf('\n -- Preparing current FOV for RCC-based drift correction ... \n')   
% 
%         % Generate Coords variable as input for RCC
%         % Remove NaN and Inf
%         
%         switch Localizer 
%             case "Fang" 
%             coords(:,1) = (locs(:,locsColumns.xCol))/pixel_size; % coords in pixels
%             coords(:,2) = (locs(:,locsColumns.yCol))/pixel_size;
%         
%             case "The other one"
%             coords(:,1) = locs(:,locsColumns.xCol_px);
%             coords(:,2) = locs(:,locsColumns.yCol_px);
%         end
%             
%         coords(:,3) = locs(:,locsColumns.frameCol);
% 
%         % Remove NaN and Inf
%         temp    = coords;
%         clear coords
%         coords  = temp( ~any( isnan( temp(:,1) ) | isinf( temp(:,1) ), 2 ),: );  % This variable contains the x and y coordinates and the corresponding frame 
% 
%         fprintf('\n -- Ready for drift correction -- \n')
% 
% 
%         %% Select region to correct
% 
%     %     pxlsize = 1;
%     % 
%     %     heigth  = round((max(coords(:,2))-min(coords(:,2)))/pxlsize);
%     %     width   = round((max(coords(:,1))-min(coords(:,1)))/pxlsize);
%     %     im      = hist3([coords(:,1),coords(:,2)],[width heigth]); % heigth x width
%     % 
%     %     % Select rectangles
%     % 
%     %     rect = []; 
%     % 
%     %     figure('Position',[100 200 600 600])
%     %     f = imagesc(im, [0 10]);
%     %     %f = imagesc(imrotate(im,90), [0 10]);
%     %     %f = imagesc(imrotate(im,90),[0 2e3]);
%     %     %f = imagesc(flipdim(imrotate(im,-90),2) ,[0 10]);
%     %     colormap('parula'); colorbar;
%     %     title('Select ROI for RCC-based drift correction')
%     % 
%     %     rect = getrect;
%     % 
%     %     fprintf('\n -- ROI selected --\n')
%     % 
%     %     xmin = min(coords(:,1))+ rect(1,1)*pxlsize;
%     %     ymin = max(coords(:,2))- rect(1,2)*pxlsize - (rect(1,4)*pxlsize) ;
%     %     xmax = xmin + 512;
%     %     ymax = ymin + 512;
%     % 
%     %     target      = find(coords(:,1)>xmin & coords(:,1)<xmax & coords(:,2)>ymin & coords(:,2)<ymax);
%     %     coords_ROI  = coords(target,1:end);
%     % 
%     %     % Show cropped region
%     % 
%     %     heigth  = round((max(coords_ROI(:,2))-min(coords_ROI(:,2)))/pxlsize);
%     %     width   = round((max(coords_ROI(:,1))-min(coords_ROI(:,1)))/pxlsize);
%     %     im      = hist3([coords_ROI(:,1),coords_ROI(:,2)],[width heigth]); % heigth x width
%     % 
%     %     figure('Position',[100 200 600 600])
%     %     imagesc(im, [0 10]);
%     %     %imagesc(imrotate(im,90), [0 10]);
%     %     %imagesc(imrotate(im,90),[0 2e3]);
%     %     %imagesc(flipdim(imrotate(im,-90),2) ,[0 10]);
%     %     colormap('parula'); colorbar;
%     %     title('Selected ROI for RCC-based drift correction')
%     % 
%     %     coords_ROI(:,1) = coords_ROI(:,1)-min(coords_ROI(:,1));
%     %     coords_ROI(:,2) = coords_ROI(:,2)-min(coords_ROI(:,2));
% 
%         %% Drift correct
%         tic
% 
%         cd([fitting_dist '\RCC_drift_correct']);                                % Changes to the directory in which the code for RCC-based drift correction is saved
% 
%         % Input:    coords:             localization coordinates [x y frame], 
%         %           segpara:            segmentation parameters (time wimdow, frame)
%         %           imsize:             image size (pixel)
%         %           pixelsize:          camera pixel size (nm)
%         %           binsize:            spatial bin pixel size (nm)
%         %           rmax:               error threshold for re-calculate the drift (pixel)
%         % Output:   coordscorr:         localization coordinates after correction [xc yc] 
%         %           finaldrift:         drift curve (save A and b matrix for other analysis)
% 
%         %finaldrift = RCC_TS(filepath, 1000, 256, 160, 30, 0.2);
% 
%         segpara     = max(locs(:,locsColumns.frameCol))/5;                                 % the stack of all recorded frames is devided into 5 parts, which are each combined into one segment. The cross-correlation is calculated between the segments, the obtained drift between the segments is interpolated -> one drift value for each frame
%         imsize      = image_size;                                               % the value entered in the beginning of the script
%         pixel_size 	= 106;
%         binsize     = 30;
% 
%         [coordscorr, finaldrift] = RCC(coords, segpara, imsize, pixel_size, binsize, 0.2);       % isn't here "corrdscorr" already what we need? Compare it with coordsDC
%         %[coordscorr, finaldrift] = RCC(coords_ROI, segpara, imsize, pixelsize, binsize, 0.2);
%         %[coordscorr, finaldrift] = DCC(coords, segpara, imsize, pixelsize, binsize);
% 
%         display(['DC finished in ' round(num2str(toc/60)) ' min']);
% 
%         %% Plot Drift curves
% 
%         figure('Position',[100 100 900 400]);                                    % what are the units here? Can only be nm or pixels 
%         subplot(2,1,1);
%         plot(finaldrift(:,1));
%         xlabel('Movie frame');
%         ylabel('Drift [pixels]');
%         title('x Drift');
% 
%         subplot(2,1,2);
%         plot(finaldrift(:,2));
%         xlabel('Movie frame');
%         ylabel('Drift [pixels]');
%         title('y Drift');
% 
%         cd(savepath);                                                           % saves drift curves to the saving directory of the current field of view
%         savefig('Drift_curves');
%         close;                                                      
% 
%         %% Apply correction to coords WOULD NOT BE NECESSARY AS THE RCC CODE ALREADY GIVES THE DRIFT-CORRECTED COORDINATES BUT THE RESULTS ARE THE SAME
% 
%         deltaXY      = [];                          
%         deltaXY(:,1) = coords(:,3);                 % frames of all localisations
%         deltaXY(:,2) = finaldrift(deltaXY(:,1),1);  % x drift for all localisations (since the drift is calculated per frame the drift will be the same for the localisations in the same frame)
%         deltaXY(:,3) = finaldrift(deltaXY(:,1),2);  % y drift for all localisations (since the drift is calculated per frame the drift will be the same for the localisations in the same frame)
% 
%         coordsDC      = [];
%         coordsDC(:,1) = coords(:,1)-deltaXY(:,2);   % x coordinates - x drift (if the sample drifts in the positive direction the drift needs to be subtracted to reach the correct coordinate; if the sample drifts in the negative direction (negative value) the amount of drift needs to be added -> subtracting negative value leads to addition) 
%         coordsDC(:,2) = coords(:,2)-deltaXY(:,3);   % y coordinates - y drift
% 
%         display('Coord corrected');
% 
%         clear coords
% 
%         locs_DC                         = locs;                             % locs_DC contains all localisations with their drift-corrected coordinates
%         locs_DC(:,locsColumns.xCol_px)  = coordsDC(:,1);                    % x [pixels]
%         locs_DC(:,locsColumns.yCol_px)  = coordsDC(:,2);                    % y [pixels]
%         locs_DC(:,locsColumns.xCol)     = coordsDC(:,1)*pixel_size;         % x [nm]
%         locs_DC(:,locsColumns.yCol)     = coordsDC(:,2)*pixel_size;         % y [nm]
% 
%         % Plot localisations ...  
%         figure('Position',[300 400 800 300])
%         subplot(1,2,1);                                     % ... before drift correction 
%         scatter(locs(:,locsColumns.xCol),locs(:,locsColumns.yCol),2);
%         title('Before drift correction');
%         xlabel('x [nm]');
%         ylabel('y [nm]');
%         axis square                      
%         xlim([-5000 image_size*pixel_size+5000]); 
%         ylim([-5000 image_size*pixel_size+5000]);
%                 
%         subplot(1,2,2)                                      % ... after drift correction
%         scatter(locs_DC(:,locsColumns.xCol),locs_DC(:,locsColumns.yCol),2);
%         title('After drift correction');
%         xlabel('x [nm]');
%         ylabel('y [nm]');
%         axis square
%         xlim([-5000 image_size*pixel_size+5000]); 
%         ylim([-5000 image_size*pixel_size+5000]);
%         
%         cd(savepath);
%         gcf; savefig('Uncorrected vs. drift-corrected locs.fig'); % Save figure
%         close;

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%END%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Drift Correction%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %     %% Save the image, pre-clustered (??) saves the drift-corrected, unfiltered localisations as an image
    %     
    %     % creates an image/3D histogram out of the drift-corrected, unfiltered
    %     % localizations
    %     pxlsize = 10.6;
    %     heigth  = round((max(locs_DC(:,locsColumns.yCol))-min(locs_DC(:,locsColumns.yCol)))/pxlsize);
    %     width   = round((max(locs_DC(:,locsColumns.xCol))-min(locs_DC(:,locsColumns.xCol)))/pxlsize);
    %     im = hist3([locs_DC(:,locsColumns.yCol),locs_DC(:,locsColumns.xCol)],[heigth width]);
    % 
    %     % filters the image with a 2-D Gaussian smoothing kernel with standard deviation specified by sigma.
    %     imG = imgaussfilt(im, pxlsize/5);
    % 
    %     % shows the image gaussian blurred
    %     figure
    %     imshow(imG,[0.01 1]);
    %     title('Unfiltered drift-corrected localisations gaussian blurred')
    %     colormap('hot');
    % 
    %     % saves the unblurred image
    %     cd(savepath)
    %     I32 = [];
    %     I32 = uint32(im);   % converts the unblurred image into 32-bit
    % 
    %     t = Tiff('Unfilt_DC_locs.tiff','w');
    %     tagstruct.ImageLength     = size(I32,1);
    %     tagstruct.ImageWidth      = size(I32,2);
    %     tagstruct.Photometric     = Tiff.Photometric.MinIsBlack;
    %     tagstruct.BitsPerSample   = 32;
    %     tagstruct.SamplesPerPixel = 1;
    %     tagstruct.RowsPerStrip    = 16;
    %     tagstruct.PlanarConfiguration = Tiff.PlanarConfiguration.Chunky;
    %     tagstruct.Software        = 'MATLAB';
    %     t.setTag(tagstruct)
    % 
    %     t.write(I32);
    %     t.close()

        %% Show histograms of filter parameters for all unfiltered localisations of the current FOV

        figure('Position',[400 300 1300 700]);        
        
        switch  Localizer 
            case "Fang"           
                %logLikelyhood 
                subplot(2,2,1);
                bins = 0:15:1500;
                h1_Fang_LL_unf = hist(locs_DC(:,locsColumns.LLCol),bins);
                %bar(bins,h1/sum(h1));
                bar(bins,h1_Fang_LL_unf);
                xlabel('logLikelihood');
                ylabel('Number of localisation');
                title(['Min = ' num2str(min(locs_DC(:,locsColumns.LLCol)),'%.2f') '      Median = ' num2str(median(locs_DC(:,locsColumns.LLCol)),'%.2f') '      Max = ' num2str(max(locs_DC(:,locsColumns.LLCol)),'%.2f')]);
                %axis([0 1.1e3 0 max(h1/sum(h1))+0.01]);
                axis([0 1.5e3 0 max(h1_Fang_LL_unf)+0.2e4]);
                
                %Intensity
                subplot(2,2,2);
                bins = 0:100:10000;
                h1_Fang_Photons_unf = hist(locs_DC(:,locsColumns.intensityCol),bins);
                %bar(bins,h1/sum(h1));
                bar(bins,h1_Fang_Photons_unf);
                xlabel('Intensity [Photons]');
                ylabel('Number of localisation');
                title(['Min = ' num2str(min(locs_DC(:,locsColumns.intensityCol)),'%.2f') '      Median = ' num2str(median(locs_DC(:,locsColumns.intensityCol)),'%.2f') '      Max = ' num2str(max(locs_DC(:,locsColumns.intensityCol)),'%.2f')]);
                %axis([0 1.1e4 0 max(h1/sum(h1))+0.01]);
                axis([0 1.1e4 0 max(h1_Fang_Photons_unf)+1e3]);

                %Uncertainty
                subplot(2,2,3);
                bins = 0:2:200;
                h1_Fang_Uncert_unf = hist(locs_DC(:,locsColumns.uncertCol),bins);
                %bar(bins,h1/sum(h1));
                bar(bins,h1_Fang_Uncert_unf);
                xlabel('Uncertainty [nm]');
                ylabel('Number of localisation');
                title(['Min = ' num2str(min(locs_DC(:,locsColumns.uncertCol)),'%.2f') '      Median = ' num2str(median(locs_DC(:,locsColumns.uncertCol)),'%.2f') '      Max = ' num2str(max(locs_DC(:,locsColumns.uncertCol)),'%.2f')]);
                %axis([-5 220 0 max(h1/sum(h1))+0.01]);
                axis([-5 220 0 max(h1_Fang_Uncert_unf)+0.2e4]);

                %Sigma
                subplot(2,2,4);
                bins = 0:5:550;
                h1_Fang_Sigma_unf = hist(locs_DC(:,locsColumns.sigmaCol),bins);
                %bar(bins,h1/sum(h1));
                bar(bins,h1_Fang_Sigma_unf);
                xlabel('Sigma [nm]');
                ylabel('Number of localisation');
                title(['Min = ' num2str(min(locs_DC(:,locsColumns.sigmaCol)),'%.2f') '      Median = ' num2str(median(locs_DC(:,locsColumns.sigmaCol)),'%.2f') '      Max = ' num2str(max(locs_DC(:,locsColumns.sigmaCol)),'%.2f')]);
                %axis([0 550 0 max(h1/sum(h1))+0.01]);
                axis([0 550 0 max(h1_Fang_Sigma_unf)+0.2e4]);
                %close
            
                sgtitle('Properties of all localisations of the FOV before filtering (Fang localizer)');
                
            case "The other one"            
                %logLikelihood 
                subplot(2,2,1);
                bins = -1000:50:0;
                h1_Too_LL_unf = hist(locs_DC(:,locsColumns.LLCol),bins);
                bar(bins,h1_Too_LL_unf);
                xlabel('logLikelihood');
                ylabel('Number of localisation');
                title(['Min = ' num2str(min(locs_DC(:,locsColumns.LLCol)),'%.2f') '      Median = ' num2str(median(locs_DC(:,locsColumns.LLCol)),'%.2f') '      Max = ' num2str(max(locs_DC(:,locsColumns.LLCol)),'%.2f')]);
                axis([-1000 0 0 (max(h1_Too_LL_unf)+5000)]);

                %Photons
                subplot(2,2,2);
                bins = 0:200:10000;
                h1_Too_photons_unf = hist(locs_DC(:,locsColumns.photonsCol),bins);
                bar(bins,h1_Too_photons_unf);
                xlabel('Photons');
                ylabel('Number of localisation');
                title(['Min = ' num2str(min(locs_DC(:,locsColumns.photonsCol)),'%.2f') '     Median = ' num2str(median(locs_DC(:,locsColumns.photonsCol)),'%.2f') '      Max = ' num2str(max(locs_DC(:,locsColumns.photonsCol)),'%.2f')]);
                axis([0 1e4 0 (max(h1_Too_photons_unf)+5000)]);

                %Z coordinate [nm]
                if isempty(locsColumns.zCol)==1

                    subplot(2,2,3);
                    title('No Z column found');

                else

                    subplot(2,2,3);
                    bins = -2000:200:2000;
                    h1_Too_z_unf = hist(locs_DC(:,locsColumns.zCol),bins);
                    bar(bins,h1_Too_z_unf);
                    xlabel('Z position');
                    ylabel('Number of localisation');
                    title(['Min = ' num2str(min(locs_DC(:,locsColumns.zCol)),'%.2f') '    Median = ' num2str(median(locs_DC(:,locsColumns.zCol)),'%.2f') '      Max = ' num2str(max(locs_DC(:,locsColumns.zCol)),'%.2f')]);
                    axis([-2000  2000 0 max(h1_Too_z_unf)]);

                end       

                %crlb_background 
                subplot(2,2,4);
                bins = 0:1:30;
                h1_Too_bg_unf = hist(locs_DC(:,locsColumns.bgCol),bins);
                bar(bins,h1_Too_bg_unf);
                xlabel('Background');
                ylabel('Number of localisation');
                title(['Min = ' num2str(min(locs_DC(:,locsColumns.bgCol)),'%.2f') '      Median = ' num2str(median(locs_DC(:,locsColumns.bgCol)),'%.2f') '      Max = ' num2str(max(locs_DC(:,locsColumns.bgCol)),'%.2f')]);
                axis([-2 10 0 (max(h1_Too_bg_unf)+5000)]);                      
                
                sgtitle('Properties of all localisations of the FOV before filtering ("The other" localizer)');
        end
         
        cd(savepath);
        savefig(strcat('Prop_all_locs_FOV_', num2str(current_FOV), '_before_filt'));
        close;
        
        %% Filter localisations of the current entire FOV
        %locs_current_entire_FOV_filtered = filter_localisations(locs_DC, Localizer, locsColumns, 'Yes', savepath);
        locs_current_entire_FOV_filtered = filter_localisations(locs_DC, Localizer, locsColumns, 'Yes', savepath);
        
        locs_current_entire_FOV_filtered_framed = Add_frame_around_locs(locs_current_entire_FOV_filtered, frame_size, locsColumns);
        
        Write_Thunderstorm_locs(locs_current_entire_FOV_filtered_framed, locsColumns, '2D', Localizer, ['FOV_', num2str(current_FOV), ' ', '_locs_TS.csv'], savepath);
        
        % Render locs of entire cell into TIFF image
        Render_SR_image(locs_current_entire_FOV_filtered_framed(:,locsColumns.xCol), locs_current_entire_FOV_filtered_framed(:,locsColumns.yCol), SR_pixel_size, ['FOV_', num2str(current_FOV), '_', num2str(SR_pixel_size), 'nmpxsz'], savepath);
        

         %% Show histograms of filter parameters for all filtered localisations of the current FOV
        
        figure('Position',[400 300 1300 700]);        
        
        switch  Localizer 
           case "Fang"           
                %logLikelihood 
                subplot(2,2,1);
                bins = 0:15:1500;
                h1_Fang_LL_f = hist(locs_current_entire_FOV_filtered(:,locsColumns.LLCol),bins);
                %bar(bins,h1/sum(h1));
                bar(bins,h1_Fang_LL_f);
                xlabel('logLikelihood');
                ylabel('Number of localisation');
                title(['Min = ' num2str(min(locs_current_entire_FOV_filtered(:,locsColumns.LLCol)),'%.2f') '      Median = ' num2str(median(locs_current_entire_FOV_filtered(:,locsColumns.LLCol)),'%.2f') '      Max = ' num2str(max(locs_current_entire_FOV_filtered(:,locsColumns.LLCol)),'%.2f')]);
                %axis([0 1.1e3 0 max(h1/sum(h1))+0.01]);
                axis([0 1.5e3 0 max(h1_Fang_LL_unf)+0.2e4]);

                %Photons
                subplot(2,2,2);
                bins = 0:100:10000;
                h1_Fang_Photons_f = hist(locs_current_entire_FOV_filtered(:,locsColumns.intensityCol),bins);
                %bar(bins,h1/sum(h1));
                bar(bins,h1_Fang_Photons_f);
                xlabel('Intensity [Photons]');
                ylabel('Number of localisation');
                title(['Min = ' num2str(min(locs_current_entire_FOV_filtered(:,locsColumns.intensityCol)),'%.2f') '      Median = ' num2str(median(locs_current_entire_FOV_filtered(:,locsColumns.intensityCol)),'%.2f') '      Max = ' num2str(max(locs_current_entire_FOV_filtered(:,locsColumns.intensityCol)),'%.2f')]);
                %axis([0 1.1e4 0 max(h1/sum(h1))+0.01]);
                axis([0 1.1e4 0 max(h1_Fang_Photons_unf)+1e3]);

                %Uncertainty
                subplot(2,2,3);
                bins = 0:2:200;
                h1_Fang_Uncert_f = hist(locs_current_entire_FOV_filtered(:,locsColumns.uncertCol),bins);
                %bar(bins,h1/sum(h1));
                bar(bins,h1_Fang_Uncert_f);
                xlabel('Uncertainty [nm]');
                ylabel('Number of localisation');
                title(['Min = ' num2str(min(locs_current_entire_FOV_filtered(:,locsColumns.uncertCol)),'%.2f') '      Median = ' num2str(median(locs_current_entire_FOV_filtered(:,locsColumns.uncertCol)),'%.2f') '      Max = ' num2str(max(locs_current_entire_FOV_filtered(:,locsColumns.uncertCol)),'%.2f')]);
                %axis([-5 220 0 max(h1/sum(h1))+0.01]);
                axis([-5 220 0 max(h1_Fang_Uncert_unf)+0.2e4]);

                %Sigma
                subplot(2,2,4);
                bins = 0:5:550;
                h1_Fang_Sigma_f = hist(locs_current_entire_FOV_filtered(:,locsColumns.sigmaCol),bins);
                %bar(bins,h1/sum(h1));
                bar(bins,h1_Fang_Sigma_f);
                xlabel('Sigma [nm]');
                ylabel('Number of localisation');
                title(['Min = ' num2str(min(locs_current_entire_FOV_filtered(:,locsColumns.sigmaCol)),'%.2f') '      Median = ' num2str(median(locs_current_entire_FOV_filtered(:,locsColumns.sigmaCol)),'%.2f') '      Max = ' num2str(max(locs_current_entire_FOV_filtered(:,locsColumns.sigmaCol)),'%.2f')]);
                %axis([0 550 0 max(h1/sum(h1))+0.01]);
                axis([0 550 0 max(h1_Fang_Sigma_unf)+0.2e4]);                
                
                sgtitle('Properties of all localisations of the FOV after filtering (Fang localizer)');
                
            case "The other one"
                % logLikelyhood                 
                subplot(2,2,1);
                bins = -1000:50:0;
                h1_Too_LL_f = hist(locs_current_entire_FOV_filtered(:,locsColumns.LLCol),bins);
                bar(bins,h1_Too_LL_f);
                xlabel('logLikelyhood');
                ylabel('Number of localisation');
                title(['Min = ' num2str(min(locs_current_entire_FOV_filtered(:,locsColumns.LLCol)),'%.2f') '     Median = ' num2str(median(locs_current_entire_FOV_filtered(:,locsColumns.LLCol)),'%.2f') '     Max = ' num2str(max(locs_current_entire_FOV_filtered(:,locsColumns.LLCol)),'%.2f')]);
                axis([-1000 0 0 (max(h1_Too_LL_unf)+5000)]);

                %Photons
                subplot(2,2,2);
                bins = 0:200:10000;
                h1_Too_Photons_f = hist(locs_current_entire_FOV_filtered(:,locsColumns.photonsCol),bins);
                bar(bins,h1_Too_Photons_f);
                xlabel('Photons');
                ylabel('Number of localisation');
                title(['Min = ' num2str(min(locs_current_entire_FOV_filtered(:,locsColumns.photonsCol)),'%.2f') '    Median = ' num2str(median(locs_current_entire_FOV_filtered(:,locsColumns.photonsCol)),'%.2f') '    Max = ' num2str(max(locs_current_entire_FOV_filtered(:,locsColumns.photonsCol)),'%.2f')]);
                axis([0 1e4 0 (max(h1_Too_photons_unf)+5000)]);

                %Z coordinate [nm]
                if isempty(locsColumns.zCol)==1

                    subplot(2,2,3);
                    title('No Z column found');

                else

                    subplot(2,2,3);
                    bins = -2000:200:2000;
                    h1_Too_z_f = hist(locs_current_entire_FOV_filtered(:,locsColumns.zCol),bins);
                    bar(bins,h1_Too_z_f);
                    xlabel('Z position');
                    ylabel('Number of localisation');
                    title(['Min = ' num2str(min(locs_current_entire_FOV_filtered(:,locsColumns.zCol)),'%.2f') '     Median = ' num2str(median(locs_current_entire_FOV_filtered(:,locsColumns.zCol)),'%.2f') '   Max = ' num2str(max(locs_current_entire_FOV_filtered(:,locsColumns.zCol)),'%.2f')]);
                    axis([-2000  2000 0 max(h1_Too_z_unf)]);

                end

                %crlb_background 
                subplot(2,2,4);
                bins = 0:1:30;
                h1_Too_bg_f = hist(locs_current_entire_FOV_filtered(:,locsColumns.bgCol),bins);
                bar(bins,h1_Too_bg_f);
                xlabel('Background');
                ylabel('Number of localisation');
                title(['Min = ' num2str(min(locs_current_entire_FOV_filtered(:,locsColumns.bgCol)),'%.2f') '     Median = ' num2str(median(locs_current_entire_FOV_filtered(:,locsColumns.bgCol)),'%.2f') '     Max = ' num2str(max(locs_current_entire_FOV_filtered(:,locsColumns.bgCol)),'%.2f')]);
                axis([-2 10 0 (max(h1_Too_bg_unf)+5000)]);                
                
                sgtitle('Properties of all localisations of the FOV after filtering ("The other" localizer)');
        end        
       
        cd(savepath);
        savefig(strcat('Prop_all_locs_FOV_', num2str(current_FOV), '_after_filt'));
        close;

        %% Iterates over all selected cells within the current field of view 
        for current_cell = 1:numel(fieldnames(batch_database.(strcat('FOV_', num2str(current_FOV))).Cells))

            % Gets locs belonging to the current entire cell 
            locs_current_entire_cell = locs_DC(batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).locs.entire_cell.logicals,:);

            % Filter locs belonging to the current entire cell 
            locs_current_entire_cell_filtered = filter_localisations(locs_current_entire_cell, Localizer, locsColumns, 'No', savepath);

            % Adds frame around locs (necessary for rendering)
            locs_current_entire_cell_filtered_framed = Add_frame_around_locs(locs_current_entire_cell_filtered, frame_size, locsColumns);
            
            % Saves the filtered localisations within the current entire cell 
            batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).locs.entire_cell.filtered_locs = locs_current_entire_cell_filtered;
            % Saves the filtered localisations within the current entire cell (framed)
            batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).locs.entire_cell.filtered_locs_framed = locs_current_entire_cell_filtered_framed;
            % Saves the filtered localisations within the current entire cell (framed) as a ThunderStorm file
            Write_Thunderstorm_locs(locs_current_entire_cell_filtered_framed, locsColumns, '2D', Localizer, strcat('Cell_', num2str(current_cell), '_entire_cell_locs_TS.csv'), savepath);
            figure; scatter(locs_current_entire_cell_filtered(:,locsColumns.xCol),locs_current_entire_cell_filtered(:,locsColumns.yCol),2); title(['Cell ', ' ', num2str(current_cell)]); xlabel('x [nm]'); ylabel('y [nm]'); axis square; xlim([0 image_size*pixel_size]); ylim([0 image_size*pixel_size]); box on; cd(savepath); savefig(strcat('Cell_', num2str(current_cell), '_(filtered_locs_entire_cell)')); close;

            % Saves the number of localisations within the current entire cell 
            batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).locs.entire_cell.no_of_filtered_locs = length(locs_current_entire_cell_filtered);

            % Saves the area of the current entire cell 
            boundary_current_entire_cell = boundary(locs_current_entire_cell_filtered(:,locsColumns.xCol),locs_current_entire_cell_filtered(:,locsColumns.yCol), 0.15);
            %conv_hull_current_cell = convhull(locs_current_entire_cell_filtered(:,locsColumns.xCol),locs_current_entire_cell_filtered(:,locsColumns.yCol));       % Convex hull of entire cell 
            %figure; plot(locs_current_entire_cell_filtered(conv_hull_current_cell,locsColumns.xCol), locs_current_entire_cell_filtered(conv_hull_current_cell,locsColumns.yCol),'r-',locs_current_entire_cell_filtered(conv_hull_current_cell,locsColumns.xCol),locs_current_entire_cell_filtered(conv_hull_current_cell,locsColumns.yCol),'b*');hold on; scatter(locs_current_entire_cell_filtered(:,locsColumns.xCol),locs_current_entire_cell_filtered(:,locsColumns.yCol),2); cd(savepath); savefig(strcat('Cell_', num2str(current_cell),'_+_convex_hull')); close;
            %x_y_locs_current_entire_cell = locs_current_entire_cell(:,1:2);                                            % Saves only the x and y coordinates of the localisations belonging to the current cell
            area_current_cell = polyarea(locs_current_entire_cell_filtered(boundary_current_entire_cell,locsColumns.xCol),locs_current_entire_cell_filtered(boundary_current_entire_cell,locsColumns.yCol));  % Area of the current entire cell in nm^2
            %area_current_cell = polyarea(locs_current_entire_cell_filtered(conv_hull_current_cell,locsColumns.xCol),locs_current_entire_cell_filtered(conv_hull_current_cell,locsColumns.yCol));  % Area of the current entire cell in nm^2
            %area_current_cell = polyarea(x_y_locs_current_entire_cell(conv_hull_current_cell,1),x_y_locs_current_entire_cell(conv_hull_current_cell,2));
            batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).locs.entire_cell.area = area_current_cell;
            %Plots the current entire cell + its area 
            figure; plot(locs_current_entire_cell_filtered(boundary_current_entire_cell,locsColumns.xCol), locs_current_entire_cell_filtered(boundary_current_entire_cell,locsColumns.yCol),'r-');hold on; scatter(locs_current_entire_cell_filtered(:,locsColumns.xCol),locs_current_entire_cell_filtered(:,locsColumns.yCol),2); title(['Cell',' ', num2str(current_cell), ' ', '+ boundary' newline 'Cell area:',' ', num2str((area_current_cell/1000000),'%.2f'),' ','�m^2']); xlabel('x [nm]'); ylabel('y [nm]'); axis square; xlim([0 image_size*pixel_size]); ylim([0 image_size*pixel_size]); box on; cd(savepath); savefig(strcat('Cell_', num2str(current_cell),'_+_boundary')); close;
            
            % Render locs of entire cell into TIFF image
            Render_SR_image(locs_current_entire_cell_filtered_framed(:,locsColumns.xCol), locs_current_entire_cell_filtered_framed(:,locsColumns.yCol), SR_pixel_size, strcat('Cell_', num2str(current_cell), '_entire_cell_', num2str(SR_pixel_size), 'nmpxsz'), savepath);

               
            
            
            
            
            switch isfield(batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).locs, 'specks')  
                
                case 0 % if the current cell does not contain a speck the entire cell is scanned for clusters
                % DBSCAN localisations in the cytoplasma of cells WITHOUT speck
                % Normalizes localizations for DBSCAN analysis to 0
                dataDBS      = [];
                %Scaled to 0 previously
                %dataDBS(:,1) = locs_current_entire_cell_filtered(:,locsColumns.xCol)-min(locs_current_entire_cell_filtered(:,locsColumns.xCol)); % x in nm              
                %dataDBS(:,2) = locs_current_entire_cell_filtered(:,locsColumns.yCol)-min(locs_current_entire_cell_filtered(:,locsColumns.yCol)); % y in nm
                dataDBS(:,1) = locs_current_entire_cell_filtered(:,locsColumns.xCol);   % x in nm 
                dataDBS(:,2) = locs_current_entire_cell_filtered(:,locsColumns.yCol);   % y in nm 
                
                % Specifies input parameters for DBSCAN
                k_dbscan = 10;                                             % minimum number of neighbors within Eps
                Eps      = 70;                                             % maximum distance between points in order for them to be counted as neighbors, nm 

                % Runs the DBSCAN algorithm on the data
                [class,type]=DBSCAN(dataDBS,k_dbscan,Eps);                  % uses parameters specified at input
                class2=transpose(class);                                    % class - vector specifying assignment of the i-th point to certain cluster (m,1)
                type2=transpose(type);                                      % type - vector specifying type of the i-th point(core: 1, border: 0, outlier: -1)

                coreBorder = [];
                coreBorder = find(type2 >= 0);                              % gets the positions of all localisations belonging to a cluster (either as a core or a border point)

                cluster_locs          = [];
                cluster_locs          = locs_current_entire_cell_filtered(coreBorder,1:end); % contains all localisations that are either a core or a border point 
                cluster_locs(:,end+1) = class2(coreBorder);         % adds for every localization belonging to a cluster to which cluster it belongs (cluster ID) 

                cluster_locs_x_y_ID         = [];                         % contains for all localizations belonging to a cluster the x coordinate, y coordinate and cluster ID
                cluster_locs_x_y_ID(:,1)    = dataDBS(coreBorder,1);      % x coordinate
                cluster_locs_x_y_ID(:,2)    = dataDBS(coreBorder,2);      % y coordinate 
                cluster_locs_x_y_ID(:,3)    = class2(coreBorder);         % cluster ID

                % Save only locs belonging to clusters 
                batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).locs.entire_cell.clusters_x_y_ID = cluster_locs_x_y_ID;

                % Measurments on every cluster:
                % The following could be used if clusters should be filtered (e.g. to exclude fiducials)
                % max_length = 5000; % high enough to exclude fiducials
                % filtered_clusters = [];

                no_locs_per_cluster = [];                                       % Initializes the vector
                area_per_cluster    = [];                                       % Initializes the vector
                for i = 1:max(cluster_locs_x_y_ID(:,3))                         % Iterates over all clusters 

                    cluster_indices = find(cluster_locs_x_y_ID(:,3)==i);        % Find the indices of all localisations belonging to the i-th cluster 

                    current_cluster = [];
                    current_cluster = cluster_locs_x_y_ID(cluster_indices,1:end);    % Contains only the localisations belonging to the i-th cluster

                    % To filter clusters 
                    %if size(target,1)<max_length==1                                  % If the number of localisations in a cluster is smaller than the maximum value given above

                    %   filtered_clusters = vertcat(filtered_clusters, cluster);      % Let this cluster remain 

                    %end

                    % Number of locs per cluster
                    no_locs_per_cluster(i,1) = length(current_cluster);           % Contains for every cluster the number of localisations        
                    no_locs_per_cluster(i,2) = i;
                    
                    % Area per cluster
                    conv_hull_current_cluster = convhull(current_cluster(:,1), current_cluster(:,2));   % Calculates the convex hull of the i-th cluster
                    conv_hull_of_each_cluster {i,1} = conv_hull_current_cluster;    % Convex hull current cluster
                    conv_hull_of_each_cluster {i,2} = i;                            % Cluster ID
                    area_current_cluster = polyarea(current_cluster(conv_hull_current_cluster,1),current_cluster(conv_hull_current_cluster,2));         % Computes the area of the convex hull of the i-th cluster [nm^2]
                    area_per_cluster(i,1) = area_current_cluster;                                       % Contains for every cluster the area in nm^2
                    area_per_cluster(i,2) = i;                                                          % Cluster ID 

                end

                % Mean number of localisations per cluster
                mean_no_locs_per_cluster_entire_cell = mean(no_locs_per_cluster(:,1));
                % Save (mean) number of locs per cluster
                batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).locs.entire_cell.no_locs_per_cluster_ID = no_locs_per_cluster;
                batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).locs.entire_cell.mean_no_locs_per_cluster = mean_no_locs_per_cluster_entire_cell;

                % Mean area of clusters 
                mean_area_per_cluster_entire_cell = mean(area_per_cluster(:,1)); 
                % Save (mean) area per cluster 
                batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).locs.entire_cell.area_per_cluster_ID = area_per_cluster;
                batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).locs.entire_cell.mean_area_per_cluster = mean_area_per_cluster_entire_cell;


                cluster_density_entire_cell = max(cluster_locs_x_y_ID(:,3))/area_current_cell;     % Calculates: total number of clusters/area of the cytoplasma 
                % Save cluster density in cytoplasma
                batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).locs.entire_cell.cluster_density = cluster_density_entire_cell;


                
                
                % Plots results of cluster analysis in cytoplasma of cells
                % WO speck
                
                % Plots the filtered localisations before DBSCAN
                %figure('Position',[0 0 9000 9000]);
                figure('Position', get(0, 'Screensize'));
                subplot(2,3,1)
                scatter(dataDBS(:,1),dataDBS(:,2),1);
                title(['Localizations before DBSCAN' newline 'Localisation density: ' num2str(length(locs_current_entire_cell_filtered)/(area_current_cell/1000000),'%.2f') ' locs/�m^2']);
                %axis([min(dataDBS(:,1)) max(dataDBS(:,1)) min(dataDBS(:,2)) max(dataDBS(:,2))])    
                xlabel('x [nm]');
                ylabel('y [nm]');
                axis square                
                xlim([0 image_size*pixel_size]); 
                ylim([0 image_size*pixel_size]);
                box on                 

                % Plots only the localisations belonging to a cluster 
                subplot(2,3,2)
                scatter(cluster_locs_x_y_ID(:,1),cluster_locs_x_y_ID(:,2),1,mod(cluster_locs_x_y_ID(:,3),30))
                title(['Identified clusters by DBSCAN' newline 'Cluster density: ' num2str(max(cluster_locs_x_y_ID(:,3))/(area_current_cell/1000000),'%.2f') ' clusters/�m^2'])
                axis on
                %axis([min(dataDBS(:,1)) max(dataDBS(:,1)) min(dataDBS(:,2)) max(dataDBS(:,2))])                
                xlabel('x [nm]');
                ylabel('y [nm]');
                axis square
                xlim([0 image_size*pixel_size]); 
                ylim([0 image_size*pixel_size]);
                box on
                
                % Plots the filtered localisations before DBSCAN + the
                % localisations belonging to a cluster in a single plot
                subplot(2,3,3)
                scatter(dataDBS(:,1),dataDBS(:,2),1,'b'); hold on;
                scatter(cluster_locs_x_y_ID(:,1),cluster_locs_x_y_ID(:,2),1,'r');
                title(['Localizations before DBSCAN (blue) + ' newline 'identified clusters by DBSCAN (red)']);
                %axis([min(dataDBS(:,1)) max(dataDBS(:,1)) min(dataDBS(:,2)) max(dataDBS(:,2))])    
                xlabel('x [nm]');
                ylabel('y [nm]');
                axis square                
                xlim([0 image_size*pixel_size]); 
                ylim([0 image_size*pixel_size]);
                box on  

                % Plots only the localisations belonging to a cluster together with their convex hull
                subplot(2,3,4)
                scatter(cluster_locs_x_y_ID(:,1),cluster_locs_x_y_ID(:,2),1,mod(cluster_locs_x_y_ID(:,3),30)); 
                hold on;

                for current_convex_hull = 1:max(cluster_locs_x_y_ID(:,3))

                    cluster_indices = find(cluster_locs_x_y_ID(:,3)==current_convex_hull);             % Find the indices of all localisations belonging to the i-th cluster 

                    current_cluster = [];
                    current_cluster = cluster_locs_x_y_ID(cluster_indices,1:end);                       % Contains only the localisations belonging to the i-th cluster

                    current_hull = conv_hull_of_each_cluster {current_convex_hull,1};

                    plot(current_cluster(current_hull,1), current_cluster(current_hull,2), '-r');

                end

                title('Identified clusters by DBSCAN + convex hull')
                axis on
                %axis([min(dataDBS(:,1)) max(dataDBS(:,1)) min(dataDBS(:,2)) max(dataDBS(:,2))])                
                xlabel('x [nm]');
                ylabel('y [nm]');
                axis square
                xlim([0 image_size*pixel_size]); 
                ylim([0 image_size*pixel_size]);
                box on
                
                subplot(2,3,5)               
                bins = 0:5:(median(no_locs_per_cluster(:,1))+3*median(no_locs_per_cluster(:,1)));
                h1_nolocs = hist(no_locs_per_cluster(:,1),bins);
                bar(bins,h1_nolocs);                
                xlabel('Number of localisations per cluster');
                ylabel('Occurrences');
                title(['Min = ' num2str(min(no_locs_per_cluster(:,1)),'%.2f') '      Median = ' num2str(median(no_locs_per_cluster(:,1)),'%.2f') '      Max = ' num2str(max(no_locs_per_cluster(:,1)),'%.2f')]);
                axis([0 max(bins(1,:))*1.1 0 max(h1_nolocs)+10]);
                axis square                
                box on   
                
                subplot(2,3,6)                
                bins = 0:200:(median(area_per_cluster(:,1))+2*median(area_per_cluster(:,1)));
                h1_area = hist(area_per_cluster(:,1),bins);
                bar(bins,h1_area);                
                xlabel('Area per cluster [nm^2]');
                ylabel('Occurrences');
                title(['Min = ' num2str(min(area_per_cluster(:,1)),'%.2f') '      Median = ' num2str(median(area_per_cluster(:,1)),'%.2f') '      Max = ' num2str(max(area_per_cluster(:,1)),'%.2f')]);
                axis([0 max(bins(1,:))*1.1 0 max(h1_area)+10]);
                xtickangle(45);
                axis square                
                box on
                
                k_string = num2str(k_dbscan);
                k_string_complete = strcat('k =',{' '},k_string);
                annotation('textbox', [0.5, 0.51, 0.1, 0], 'string', k_string_complete,'LineStyle','none');
                
                Eps_string = num2str(Eps);
                Eps_string_complete = strcat('Eps =',{' '},Eps_string);
                annotation('textbox', [0.5, 0.49, 0.1, 0], 'string', Eps_string_complete,'LineStyle','none');
                
                sgtitle(['Results cluster analysis on filtered localizations of Cell', ' ', num2str(current_cell), ' ','of FOV',' ', num2str(current_FOV)]);
                                
                cd(savepath);
                savefig(strcat('Cell_', num2str(current_cell),'_cluster_analysis'));
                save('k_dbscan.mat', 'k_dbscan');
                save('Eps.mat', 'Eps');
                close;
                
                
                
                

            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
                case 1 % if the current cell contains a speck only the cytoplasma is scanned for cluster                
                % Get the localisations belonging to the speck  
                locs_speck = locs_DC(batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).locs.specks.Speck_1.logicals,:);      % THIS WOULD NEED AJUSTMENT IF THERE IS MORE THAN ONE SPECK

                % Filter locs belonging to the speck
                locs_speck_filtered = filter_localisations(locs_speck, Localizer, locsColumns, 'No', savepath);

                 % Adds frame around locs (necessary for rendering)
                locs_speck_filtered_framed = Add_frame_around_locs(locs_speck_filtered, frame_size, locsColumns);
                
                % Saves the localisations of the current speck
                batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).locs.specks.Speck_1.filtered_locs = locs_speck_filtered;
                % Saves the localisations of the current speck (framed)
                batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).locs.specks.Speck_1.filtered_locs_framed = locs_speck_filtered_framed;
                % Saves the filtered localisations of the current speck (framed) as a ThunderStorm file
                Write_Thunderstorm_locs(locs_speck_filtered_framed, locsColumns, '2D', Localizer, strcat('Cell_', num2str(current_cell), '_speck_locs_TS.csv'), savepath);
                % Rescale speck coordinates
                x_locs_filtered_rescaled = locs_speck_filtered(:,locsColumns.xCol) - (mean(locs_speck_filtered(:,locsColumns.xCol))-1000);
                y_locs_filtered_rescaled = locs_speck_filtered(:,locsColumns.yCol) - (mean(locs_speck_filtered(:,locsColumns.yCol))-1000);
                figure; scatter(x_locs_filtered_rescaled,y_locs_filtered_rescaled,2); title(['Speck of Cell',' ', num2str(current_cell)]); xlabel('x [nm]'); ylabel('y [nm]'); hold on; 
                %figure; scatter(locs_speck_filtered(:,locsColumns.xCol),locs_speck_filtered(:,locsColumns.yCol),2); title(['Speck of Cell',' ', num2str(current_cell)]); xlabel('x (from FOV origin) [nm]'); ylabel('y (from FOV origin) [nm]'); cd(savepath); axis square; box on; savefig(strcat('Cell_', num2str(current_cell),'_Speck')); close;
                axis square; 
                xlim([(mean(x_locs_filtered_rescaled)-1000) (mean(x_locs_filtered_rescaled)+1000)]); 
                ylim([(mean(y_locs_filtered_rescaled)-1000) (mean(y_locs_filtered_rescaled)+1000)]);
                box on;
                cd(savepath); savefig(strcat('Cell_', num2str(current_cell),'_Speck')); close;
                
                % Saves the number of localisations within the current speck
                batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).locs.specks.Speck_1.no_of_filtered_locs = length(locs_speck_filtered);

                % Speck size 
                radius_of_gyration_speck = sqrt(sum(var([locs_speck_filtered(:,locsColumns.xCol), locs_speck_filtered(:,locsColumns.yCol)],1,1)));  % Calculates the radius of gyration of the speck
                batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).locs.specks.Speck_1.rg = radius_of_gyration_speck;    % Saves the radius of gyration of the speck

                % Speck density [locs/nm^2]
                density_speck = (length(locs_speck_filtered))/(pi*(radius_of_gyration_speck)^2);     % Calculates speck density 
                batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).locs.specks.Speck_1.density_locs = density_speck;

                % Plot speck + radius of gyration                
                figure; scatter(x_locs_filtered_rescaled, y_locs_filtered_rescaled, 2); title(['Speck of Cell',' ', num2str(current_cell),' ', '+ Radius of Gyration' newline 'Rg =',' ',num2str(radius_of_gyration_speck,'%.2f'),' ','nm']); xlabel('x [nm]'); ylabel('y [nm]'); hold on;
                %figure; scatter(locs_speck_filtered(:,locsColumns.xCol), locs_speck_filtered(:,locsColumns.yCol), 2); title(['Speck of Cell',' ', num2str(current_cell),' ', '+ Radius of Gyration' newline 'Rg =',' ',num2str(radius_of_gyration_speck),' ','nm']); xlabel('x (from FOV origin) [nm]'); ylabel('y (from FOV origin) [nm]'); hold on;          
                th = 0:pi/50:2*pi;
                xunit = radius_of_gyration_speck * cos(th) + mean(x_locs_filtered_rescaled);
                yunit = radius_of_gyration_speck * sin(th) + mean(y_locs_filtered_rescaled);
                %xunit = radius_of_gyration_speck * cos(th) + mean(locs_speck_filtered(:,locsColumns.xCol));
                %yunit = radius_of_gyration_speck * sin(th) + mean(locs_speck_filtered(:,locsColumns.yCol));
                plot(xunit, yunit,'-r');                
                axis square;
                xlim([(mean(x_locs_filtered_rescaled)-1000) (mean(x_locs_filtered_rescaled)+1000)]); 
                ylim([(mean(y_locs_filtered_rescaled)-1000) (mean(y_locs_filtered_rescaled)+1000)]);
                %xlim([(mean(locs_speck_filtered(:,locsColumns.xCol))-1000) (mean(locs_speck_filtered(:,locsColumns.xCol))+1000)]); 
                %ylim([(mean(locs_speck_filtered(:,locsColumns.yCol))-1000) (mean(locs_speck_filtered(:,locsColumns.yCol))+1000)]);
                box on;
                cd(savepath); savefig(strcat('Cell_', num2str(current_cell),'_Speck + Radius of Gyration')); close;
                
                % Renders locs of speck into TIFF image
                Render_SR_image(locs_speck_filtered_framed(:,locsColumns.xCol), locs_speck_filtered_framed(:,locsColumns.yCol), SR_pixel_size, strcat('Cell_', num2str(current_cell), '_speck_', num2str(SR_pixel_size), 'nmpxsz'), savepath);
                

                
                

                % Get the localisations within the cytoplasma 
                locs_cell_wo_specks = locs_DC(batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).locs.cell_wo_specks.logicals,:);

                % Filter locs belonging to the current cell wo specks
                locs_cell_wo_specks_filtered = filter_localisations(locs_cell_wo_specks, Localizer, locsColumns, 'No', savepath);
                
                 % Adds frame around locs (necessary for rendering)
                locs_cell_wo_specks_filtered_framed = Add_frame_around_locs(locs_cell_wo_specks_filtered, frame_size, locsColumns);

                % Saves the localisations of the current cell wo specks
                batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).locs.cell_wo_specks.filtered_locs = locs_cell_wo_specks_filtered;
                % Saves the localisations of the current cell wo specks (framed)
                batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).locs.cell_wo_specks.filtered_locs_framed = locs_cell_wo_specks_filtered_framed;
                % Saves the filtered localisations of the current cell wo specks (framed) as a ThunderStorm file
                Write_Thunderstorm_locs(locs_cell_wo_specks_filtered_framed, locsColumns, '2D', Localizer, strcat('Cell_', num2str(current_cell), '_wospeck_locs_TS.csv'), savepath);
                figure; scatter(locs_cell_wo_specks_filtered(:,locsColumns.xCol),locs_cell_wo_specks_filtered(:,locsColumns.yCol),2); title(['Cell',' ', num2str(current_cell),' ', 'with speck removed']); xlabel('x [nm]'); ylabel('y [nm]'); axis square; xlim([0 image_size*pixel_size]); ylim([0 image_size*pixel_size]); box on; cd(savepath); savefig(strcat('Cell_', num2str(current_cell),'_wo_Speck')); close;

                % Saves the number of localisations within the cytoplasma of the current cell
                batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).locs.cell_wo_specks.no_of_locs = length(locs_cell_wo_specks_filtered);
                
                % Area cytoplasma [nm^2]
                area_cytoplasma = area_current_cell - (pi*(radius_of_gyration_speck)^2);        % Calculates the area of the cytoplasma excluding the speck
                % Save area of cytoplasma [nm^2]
                batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).locs.cell_wo_specks.area = area_cytoplasma;
                
                % Density of locs within the cytoplasma
                density_locs_cell_wo_specks = (length(locs_cell_wo_specks_filtered))/area_cytoplasma;
                % Save density of locs within the cytoplasma
                batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).locs.cell_wo_specks.locs_density = density_locs_cell_wo_specks;               
                               
                % Renders locs within the cytoplasma of the current cell into TIFF image
                Render_SR_image(locs_cell_wo_specks_filtered_framed(:,locsColumns.xCol), locs_cell_wo_specks_filtered_framed(:,locsColumns.yCol), SR_pixel_size, strcat('Cell_', num2str(current_cell), '_wospeck_', num2str(SR_pixel_size), 'nmpxsz'), savepath);
                        
                
                
                
                
                % DBSCAN localisations in the cytoplasma
                % Normalizes localizations for DBSCAN analysis to 0
                dataDBS      = [];
                %Scaled to 0 previously
%                 dataDBS(:,1) = locs_cell_wo_specks_filtered(:,locsColumns.xCol)-min(locs_cell_wo_specks_filtered(:,locsColumns.xCol)); % x in nm              
%                 dataDBS(:,2) = locs_cell_wo_specks_filtered(:,locsColumns.yCol)-min(locs_cell_wo_specks_filtered(:,locsColumns.yCol)); % y in nm

                dataDBS(:,1) = locs_cell_wo_specks_filtered(:,locsColumns.xCol);    % x in nm
                dataDBS(:,2) = locs_cell_wo_specks_filtered(:,locsColumns.yCol);    % y in nm
                
                % Specifies input parameters for DBSCAN
                k_dbscan = 10;                                              % minimum number of neighbors within Eps
                Eps      = 70;                                              % maximum distance between points in order for them to be counted as neighbors, nm 

                % Runs the DBSCAN algorithm on the data
                [class,type]=DBSCAN(dataDBS,k_dbscan,Eps);                  % uses parameters specified at input
                class2=transpose(class);                                    % class - vector specifying assignment of the i-th point to certain cluster (m,1)
                type2=transpose(type);                                      % type - vector specifying type of the i-th point(core: 1, border: 0, outlier: -1)

                coreBorder = [];
                coreBorder = find(type2 >= 0);                              % gets the positions of all localisations belonging to a cluster (either as a core or a border point)

                cluster_locs          = [];
                cluster_locs          = locs_cell_wo_specks_filtered(coreBorder,1:end); % contains all localisations that are either a core or a border point 
                cluster_locs(:,end+1) = class2(coreBorder);         % adds for every localization belonging to a cluster to which cluster it belongs (cluster ID) 

                cluster_locs_x_y_ID         = [];                         % contains for all localizations belonging to a cluster the x coordinate, y coordinate and cluster ID
                cluster_locs_x_y_ID(:,1)    = dataDBS(coreBorder,1);      % x coordinate
                cluster_locs_x_y_ID(:,2)    = dataDBS(coreBorder,2);      % y coordinate 
                cluster_locs_x_y_ID(:,3)    = class2(coreBorder);         % cluster ID

                % Save only locs belonging to clusters 
                batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).locs.cell_wo_specks.clusters_x_y_ID = cluster_locs_x_y_ID;

                % Measurments on every cluster:
                % The following could be used if clusters should be filtered (e.g. to exclude fiducials)
                % max_length = 5000; % high enough to exclude fiducials
                % filtered_clusters = [];

                no_locs_per_cluster = [];                                       % Initializes the vector
                area_per_cluster    = [];                                       % Initializes the vector
                for i = 1:max(cluster_locs_x_y_ID(:,3))                         % Iterates over all clusters 

                    cluster_indices = find(cluster_locs_x_y_ID(:,3)==i);        % Find the indices of all localisations belonging to the i-th cluster 

                    current_cluster = [];
                    current_cluster = cluster_locs_x_y_ID(cluster_indices,1:end);    % Contains only the localisations belonging to the i-th cluster

                    % To filter clusters 
                    %if size(target,1)<max_length==1                                  % If the number of localisations in a cluster is smaller than the maximum value given above

                    %   filtered_clusters = vertcat(filtered_clusters, cluster);      % Let this cluster remain 

                    %end

                    % Number of locs per cluster
                    no_locs_per_cluster(i,1) = length(current_cluster);           % Contains for every cluster the number of localisations        
                    no_locs_per_cluster(i,2) = i;

                    % Area per cluster
                    conv_hull_current_cluster = convhull(current_cluster(:,1), current_cluster(:,2));   % Calculates the convex hull of the i-th cluster
                    conv_hull_of_each_cluster {i,1} = conv_hull_current_cluster;    % Convex hull current cluster
                    conv_hull_of_each_cluster {i,2} = i;                            % Cluster ID
                    area_current_cluster = polyarea(current_cluster(conv_hull_current_cluster,1),current_cluster(conv_hull_current_cluster,2));         % Computes the area of the convex hull of the i-th cluster [nm^2]
                    area_per_cluster(i,1) = area_current_cluster;                                       % Contains for every cluster the area 
                    area_per_cluster(i,2) = i;                                                          % Cluster ID 

                end                


                % Mean number of localisations per cluster
                mean_no_locs_per_cluster_cell_wo_specks = mean(no_locs_per_cluster(:,1));
                % Save (mean) number of locs per cluster
                batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).locs.cell_wo_specks.no_locs_per_cluster_ID = no_locs_per_cluster;
                batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).locs.cell_wo_specks.mean_no_locs_per_cluster = mean_no_locs_per_cluster_cell_wo_specks;

                % Mean area of clusters 
                mean_area_per_cluster_cell_wo_specks = mean(area_per_cluster(:,1)); 
                % Save (mean) area per cluster 
                batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).locs.cell_wo_specks.area_per_cluster_ID = area_per_cluster;
                batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).locs.cell_wo_specks.mean_area_per_cluster = mean_area_per_cluster_cell_wo_specks;

                % Cluster density cytoplasma
                cluster_density_cytoplasma_cell_wo_specks = max(cluster_locs_x_y_ID(:,3))/area_cytoplasma;     % Calculates: total number of clusters/area of the cytoplasma [clusters/nm^2]
                % Save cluster density in cytoplasma
                batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).locs.cell_wo_specks.cluster_density = cluster_density_cytoplasma_cell_wo_specks;
                
                
                
                
                
                % Plots results of cluster analysis in cells containing a speck 
                
                % Plots the filtered localisations before DBSCAN
                %figure('Position',[0 0 9000 9000]);
                figure('Position', get(0, 'Screensize'));
                subplot(2,3,1)
                scatter(dataDBS(:,1),dataDBS(:,2),1);                
                title(['Localizations before DBSCAN' newline 'Localisation density: ' num2str(length(locs_cell_wo_specks_filtered)/(area_cytoplasma/1000000),'%.2f') ' [locs/�m^2]']);
                axis([min(dataDBS(:,1)) max(dataDBS(:,1)) min(dataDBS(:,2)) max(dataDBS(:,2))])
                axis square
                xlabel('x [nm]');
                ylabel('y [nm]');
                xlim([0 image_size*pixel_size]); 
                ylim([0 image_size*pixel_size]);
                box on

                % Plots only the localisations belonging to a cluster 
                subplot(2,3,2)
                scatter(cluster_locs_x_y_ID(:,1),cluster_locs_x_y_ID(:,2),1,mod(cluster_locs_x_y_ID(:,3),30))
                title(['Identified clusters by DBSCAN' newline 'Cluster density: ' num2str(max(cluster_locs_x_y_ID(:,3))/(area_cytoplasma/1000000),'%.2f') ' [clusters/�m^2]'])
                axis on
                axis([min(dataDBS(:,1)) max(dataDBS(:,1)) min(dataDBS(:,2)) max(dataDBS(:,2))])
                axis square
                xlabel('x [nm]');
                ylabel('y [nm]');                
                xlim([0 image_size*pixel_size]); 
                ylim([0 image_size*pixel_size]);
                box on
                
                % Plots the filtered localisations before DBSCAN + the
                % localisations belonging to a cluster in a single plot
                subplot(2,3,3)
                scatter(dataDBS(:,1),dataDBS(:,2),1); hold on;
                scatter(cluster_locs_x_y_ID(:,1),cluster_locs_x_y_ID(:,2),1,'r');
                title(['Localizations before DBSCAN (blue) + ' newline 'identified clusters by DBSCAN (red)']);
                %axis([min(dataDBS(:,1)) max(dataDBS(:,1)) min(dataDBS(:,2)) max(dataDBS(:,2))])    
                xlabel('x [nm]');
                ylabel('y [nm]');
                axis square                
                xlim([0 image_size*pixel_size]); 
                ylim([0 image_size*pixel_size]);
                box on  

                % Plots only the localisations belonging to a cluster together with their convex hull
                subplot(2,3,4)
                scatter(cluster_locs_x_y_ID(:,1),cluster_locs_x_y_ID(:,2),1,mod(cluster_locs_x_y_ID(:,3),30)); 
                hold on;

                for current_convex_hull = 1:max(cluster_locs_x_y_ID(:,3))

                    cluster_indices = find(cluster_locs_x_y_ID(:,3)==current_convex_hull);             % Find the indices of all localisations belonging to the i-th cluster 

                    current_cluster = [];
                    current_cluster = cluster_locs_x_y_ID(cluster_indices,1:end);                       % Contains only the localisations belonging to the i-th cluster

                    current_hull = conv_hull_of_each_cluster {current_convex_hull,1};

                    plot(current_cluster(current_hull,1), current_cluster(current_hull,2), '-r');

                end

                title('Identified clusters by DBSCAN + convex hull')
                axis on
                axis([min(dataDBS(:,1)) max(dataDBS(:,1)) min(dataDBS(:,2)) max(dataDBS(:,2))])
                axis square
                xlabel('x [nm]');
                ylabel('y [nm]');
                xlim([0 image_size*pixel_size]); 
                ylim([0 image_size*pixel_size]);
                box on
                
                subplot(2,3,5)               
                bins = 0:5:(median(no_locs_per_cluster(:,1))+3*median(no_locs_per_cluster(:,1)));
                h1_nolocs = hist(no_locs_per_cluster(:,1),bins);
                bar(bins,h1_nolocs);                
                xlabel('Number of localisations per cluster');
                ylabel('Occurrences');
                title(['Min = ' num2str(min(no_locs_per_cluster(:,1)),'%.2f') '      Median = ' num2str(median(no_locs_per_cluster(:,1)),'%.2f') '      Max = ' num2str(max(no_locs_per_cluster(:,1)),'%.2f')]);
                axis([0 max(bins(1,:))*1.1 0 max(h1_nolocs)+10]);
                axis square                
                box on   
                
                subplot(2,3,6)                
                bins = 0:200:(median(area_per_cluster(:,1))+2*median(area_per_cluster(:,1)));
                h1_area = hist(area_per_cluster(:,1),bins);
                bar(bins,h1_area);                
                xlabel('Area per cluster [nm^2]');
                ylabel('Occurrences');
                title(['Min = ' num2str(min(area_per_cluster(:,1)),'%.2f') '      Median = ' num2str(median(area_per_cluster(:,1)),'%.2f') '      Max = ' num2str(max(area_per_cluster(:,1)),'%.2f')]);
                axis([0 max(bins(1,:))*1.1 0 max(h1_area)+10]);
                xtickangle(45);
                axis square                
                box on
                
                k_string = num2str(k_dbscan);
                k_string_complete = strcat('k =',{' '},k_string);
                annotation('textbox', [0.5, 0.51, 0.1, 0], 'string', k_string_complete,'LineStyle','none');
                
                Eps_string = num2str(Eps);
                Eps_string_complete = strcat('Eps =',{' '},Eps_string);
                annotation('textbox', [0.5, 0.49, 0.1, 0], 'string', Eps_string_complete,'LineStyle','none');
                
                sgtitle(['Results cluster analysis on filtered localizations of Cell', ' ', num2str(current_cell), ' ','of FOV', ' ', num2str(current_FOV), ' ', 'with speck removed'], 'FontSize', 16);
                                
                cd(savepath);
                savefig(strcat('Cell_', num2str(current_cell),'_wo_speck_cluster_analysis'));
                save('k_dbscan.mat', 'k_dbscan');
                save('Eps.mat', 'Eps');
                close;
                
            end

            % Saves the analysis results of the current cell in the analysis folder of the current cell
            Results_current_cell = batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell)));
            cd(savepath);
            save(strcat('Results_Cell_',num2str(current_cell)),'Results_current_cell');
                        
            total_no_cells                              = numel(Results.Filepath_Locs(:,1)); % counts the number of entries in the table
            Results.Filepath_Locs((total_no_cells+1),1) = raw_path;
            Results.Filename_Locs((total_no_cells+1),1) = raw_name;
            Results.Cell_number((total_no_cells+1),1)   = strcat('Cell_', num2str(current_cell));
            Results.Cell_complete((total_no_cells+1),1) = batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).Cell_complete;
            Results.Size_entire_cell__um2((total_no_cells+1),1)                     = area_current_cell/1000000;
            Results.Number_of_locs_entire_cell((total_no_cells+1),1)                = length(locs_current_entire_cell_filtered);            
            
            switch analysis_type
                case 'Incl_WF'
                    Results.Filepath_WF((total_no_cells+1),1) = batch_database.(strcat('FOV_', num2str(current_FOV))).Filenames.WF_path;
                    Results.Filename_WF((total_no_cells+1),1) = batch_database.(strcat('FOV_', num2str(current_FOV))).Filenames.WF_name;
                    Results.Int_intensity_WF_entire_cell__counts((total_no_cells+1),1) = batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).WF_data.entire_cell.int_intensity;                   
                    Results.Cell_size_WF__um2((total_no_cells+1),1) = batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).WF_data.entire_cell.cell_size;
            end
            
            switch isfield(batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).locs, 'specks') 
                case 0 % if the cell does not contain a speck
                        Results.Cell_contains_speck((total_no_cells+1),1)                            = false;
                        Results.Cluster_density_entire_cell__clusters_per_um2((total_no_cells+1),1)  = cluster_density_entire_cell*1000000;
                        Results.Average_cluster_size_entire_cell__nm2((total_no_cells+1),1)          = mean_area_per_cluster_entire_cell;
                        Results.Average_number_of_locs_per_cluster_entire_cell((total_no_cells+1),1) = mean_no_locs_per_cluster_entire_cell;
                        Results.Density_of_locs_entire_cell__locs_per_um2((total_no_cells+1),1)      = length(locs_current_entire_cell_filtered)/(area_current_cell/1000000);
                
                case 1 % if the cell does contain a speck
                        Results.Cell_contains_speck((total_no_cells+1),1) = true;
                        Results.Number_of_locs_within_speck((total_no_cells+1),1) = length(locs_speck_filtered);
                        Results.Speck_size_rg__nm((total_no_cells+1),1) = radius_of_gyration_speck;
                        Results.Speck_density__no_of_locs_per_um2((total_no_cells+1),1) = density_speck*1000000;
                        Results.Number_of_locs_within_cytoplasma((total_no_cells+1),1) = length(locs_cell_wo_specks_filtered);
                        Results.Area_cytoplasma__um2((total_no_cells+1),1) = area_cytoplasma/1000000;
                        Results.Density_of_locs_within_cytoplasma__locs_per_um2((total_no_cells+1),1) = density_locs_cell_wo_specks*1000000;                
                        Results.Cluster_density_within_cytoplasma__clusters_per_um2((total_no_cells+1),1) = cluster_density_cytoplasma_cell_wo_specks*1000000;
                        Results.Average_cluster_size_within_cytoplasma__nm2((total_no_cells+1),1) = mean_area_per_cluster_cell_wo_specks;
                        Results.Average_number_of_locs_per_cluster_within_cytoplasma((total_no_cells+1),1) = mean_no_locs_per_cluster_cell_wo_specks;
                        Results.Speck_size_rg__nm_per_size_entire_cell__um2((total_no_cells+1),1) = radius_of_gyration_speck/(area_current_cell/1000000);
                        Results.Speck_density__no_of_locs_per_um2_per_size_entire_cell__um2((total_no_cells+1),1) = (density_speck*1000000)/(area_current_cell/1000000);
                        Results.Speck_size_rg__nm_per_Number_of_locs_entire_cell((total_no_cells+1),1) = radius_of_gyration_speck/length(locs_current_entire_cell_filtered);
                        Results.Speck_density__no_of_locs_per_um2_per_Number_of_locs_entire_cel((total_no_cells+1),1) = (density_speck*1000000)/length(locs_current_entire_cell_filtered);
                
                
                switch analysis_type
                    case 'Incl_WF'
                        Results.Int_intensity_WF_speck_manual__counts((total_no_cells+1),1)         = batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).WF_data.Speck_1.int_intensity.manual;
                        Results.Int_intensity_WF_cytoplasma_manual__counts((total_no_cells+1),1)    = batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).WF_data.cell_wo_specks.int_intensity.manual;
                        Results.Speck_size_WF_manual__um2((total_no_cells+1),1)                     = batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).WF_data.Speck_1.size.manual;
                        Results.Cytoplasma_size_WF_manual__um2((total_no_cells+1),1)                = batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).WF_data.cell_wo_specks.size.manual; 
                        Results.Int_intensity_WF_speck_circular__counts((total_no_cells+1),1)       = batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).WF_data.Speck_1.int_intensity.circular;
                        Results.Int_intensity_WF_cytoplasma_circular__counts((total_no_cells+1),1)  = batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).WF_data.cell_wo_specks.int_intensity.circular;
                        Results.Speck_size_WF_circular__um2((total_no_cells+1),1)                   = batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).WF_data.Speck_1.size.circular;
                        Results.Cytoplasma_size_WF_circular__um2((total_no_cells+1),1)              = batch_database.(strcat('FOV_', num2str(current_FOV))).Cells.(strcat('Cell_', num2str(current_cell))).WF_data.cell_wo_specks.size.circular; 

                end
                
            end
            
        end

    %     %% HERE I HAVE TO SOMEHOW ITTERATE OVER THE PREVIOUSLY SELECTED ROIs AND GENERATE A NAME OUT OF IT!!
    %     
    %     %% Find all localisations within the selected ROIs
    %     
    %     all_rois = zeros(length(locs),1); % preallocate vector 
    %     
    %     for current_ROI = 1:length(batch_database(current_FOV).CellROI(1,:)) % iterates over all selected ROIs in the current FOV
    %         current_position = find(batch_database(current_FOV).CellROI(:,current_ROI) == 1);
    %         all_rois(current_position,1)=1;
    %     end
    %     
    %     all_rois_position = find(all_rois(:,1) == 1);   % contains the indices of all localizations within the ROI (indices of the unfiltered localizations)
    %     locs_DC_ROI = locs_DC(all_rois_position,1:end); % Contains only the localisations within the selected ROIs
    %     
    %     %% Apply filter parameters to localisations within current ROI
    % 
    %     filter              = [];
    %     
    %     % filter   = find(locs_DC(:,locsColumns.photonsCol) > minPhotons & locs_DC(:,uncertaintlocsColumns.yCol) < Maxuncertainty & locs_DC(:,locsColumns.frameCol) > minFrame & locs_DC(:,locsColumns.LLCol) < logLikelihood);
    %     filter              = find(locs_DC_ROI(:,locsColumns.photonsCol) > minPhotons & locs_DC_ROI(:,locsColumns.photonsCol) < maxPhotons & locs_DC_ROI(:,locsColumns.LLCol) > minLL ... 
    %                               & locs_DC_ROI(:,locsColumns.frameCol) > minFrame & locs_DC_ROI(:,4) > z_min & locs_DC_ROI(:,4) < z_max & locs_DC_ROI(:,locsColumns.bgCol) < BG_max ...
    %                               & locs_DC_ROI(:,locsColumns.frameCol) < maxFrame);
    %     
    %     locs_Filtered       = locs_DC_ROI(filter,1:end);
    %     
    % %     filter              = find(locs_DC(:,locsColumns.photonsCol) > minPhotons & locs_DC(:,locsColumns.photonsCol) < maxPhotons & locs_DC(:,locsColumns.LLCol) > minLL ... 
    % %                              & locs_DC(:,locsColumns.frameCol) > minFrame & locs_DC(:,4) > z_min & locs_DC(:,4) < z_max & locs_DC(:,locsColumns.bgCol) < BG_max ...
    % %                              & locs_DC(:,locsColumns.frameCol) < maxFrame);
    % %     locs_Filtered = locs_DC(filter,1:end);
    % 
    %     fprintf('\n -- Locs filtered %f of the locs are left --\n', ((length(locs_Filtered)/length(locs_DC))));
    % 
    %     %% Show and save filtered, drift-corrected localisations
    % 
    %     % HERE I HAVE TO SOMEHOW GET THE NAME OF THE CURRENT SELECTION TO SAVE
    %     % THE DATA 
    %     
    %     pxlsize = 10.6;                                                         % bins the localisations in bins of 10.6 
    % 
    %     heigth  = round((max(locs_DC(:,locsColumns.yCol))-min(locs_DC(:,locsColumns.yCol)))/pxlsize);
    %     width   = round((max(locs_DC(:,locsColumns.xCol))-min(locs_DC(:,locsColumns.xCol)))/pxlsize);
    % 
    %     im = hist3([locs_Filtered(:,locsColumns.yCol),locs_Filtered(:,locsColumns.xCol)],[heigth width]);
    % 
    %     % filters the image with a 2-D Gaussian smoothing kernel with standard deviation of the pixel size devided by 5
    %     imG = imgaussfilt(im, pxlsize/5);
    % 
    %     figure
    %     imshow(imG,[0.01 1]);
    %     title('Filtered drift-corrected localisations gaussian blurred')
    %     colormap('hot');
    % 
    %     cd(savepath)
    % 
    %     I32 = [];
    %     I32 = uint32(imG);
    %     %I32 = uint32(im);
    % 
    %     %t = Tiff('Filt_DC_locs.tiff','w');
    %     t = Tiff('Filt_DC_locs_gaussian_blurred.tiff','w');
    %     tagstruct.ImageLength     = size(I32,1);
    %     tagstruct.ImageWidth      = size(I32,2);
    %     tagstruct.Photometric     = Tiff.Photometric.MinIsBlack;
    %     tagstruct.BitsPerSample   = 32;
    %     tagstruct.SamplesPerPixel = 1;
    %     tagstruct.RowsPerStrip    = 16;
    %     tagstruct.PlanarConfiguration = Tiff.PlanarConfiguration.Chunky;
    %     tagstruct.Software        = 'MATLAB';
    %     t.setTag(tagstruct)
    % 
    %     t.write(I32);
    %     t.close()
    % 
    %     %% Select an ROI for DBSCAN analysis
    % 
    % %     figure('Position',[1200 400 500 500])
    % %     scatter(locs_Filtered(:,locsColumns.xCol),locs_Filtered(:,locsColumns.yCol),'.')
    % %     title('Select ROI for DBSCAN analysis');
    % %     rect = getrect;
    % % 
    % % 
    % %     % % Plot the data
    % %     % % Show 2D histogram 
    % %     % 
    % %     % %close all
    % %     % % 
    % %     % % figure('Position',[100 400 500 500])
    % %     % % scatter(locs(:,xcol),locs(:,ycol),1);
    % %     % 
    % %     % pxlsize = 200; %bins the localizations into bins of 200
    % %     % 
    % %     % heigth  = round((max(locs_DC(:,locsColumns.yCol))-min(locs_DC(:,locsColumns.yCol)))/pxlsize);
    % %     % width   = round((max(locs_DC(:,locsColumns.xCol))-min(locs_DC(:,locsColumns.xCol)))/pxlsize);
    % %     % 
    % %     % figure('Position',[650 400 500 500])
    % %     % im=hist3([locs_Filtered(:,locsColumns.xCol),locs_Filtered(:,locsColumns.yCol)],[width heigth]); % heigth x width
    % %     % imagesc(im ,[0 10]);
    % %     % %imagesc(flipdim(imrotate(im,-90),2) ,[0 10]);
    % %     % title('Select ROI for DBSCAN analysis')
    % %     % %imagesc(im,[0 200]);
    % %     % colormap('hot');
    % %     % % colorbar
    % %     % rect = getrect; % rect = [xmin ymin width height];
    % %     % 
    % %     % %close all
    % % 
    % %     fprintf('\n -- ROI for DBSCAN selected --\n')
    % % 
    % % 
    % %     % Plot selected ROI 
    % %     %close all
    % % 
    % %     xmin = min(locs_Filtered(:,locsColumns.xCol)) + rect(:,1);
    % %     ymin = min(locs_Filtered(:,locsColumns.yCol)) + rect(:,2);
    % %     % %ymin = max(locs_Filtered(:,locsColumns.yCol)) - rect(:,2)*pxlsize - (rect(:,4)*pxlsize) ;
    % %     xmax = xmin + rect(:,3);
    % %     % %ymax = ymin + rect(:,3) * pxlsize;
    % %     ymax = ymin + rect(:,4);
    % % 
    % % 
    % % 
    % % 
    % %     % xmin = min(locs_Filtered(:,locsColumns.xCol))+ rect(:,1)*pxlsize;
    % %     % ymin = min(locs_Filtered(:,locsColumns.yCol)) + rect(:,2)*pxlsize;
    % %     % %ymin = max(locs_Filtered(:,locsColumns.yCol)) - rect(:,2)*pxlsize - (rect(:,4)*pxlsize) ;
    % %     % xmax = xmin + (rect(:,3)* pxlsize);
    % %     % %ymax = ymin + rect(:,3) * pxlsize;
    % %     % ymax = ymin + rect(:,4) * pxlsize;
    % % 
    % %     % xmin=min(locs(:,3));
    % %     % xmax=max(locs(:,3)); 
    % %     % ymin=min(locs(:,4));
    % %     % ymax=max(locs(:,4));
    % % 
    % %     vx      = find(locs_Filtered(:,locsColumns.xCol)>xmin & locs_Filtered(:,locsColumns.xCol)<xmax & locs_Filtered(:,locsColumns.yCol)>ymin & locs_Filtered(:,locsColumns.yCol)<ymax);
    % %     locs_ROI = locs_Filtered(vx,1:end);
    % % 
    % % 
    % %     figure('Position',[1200 400 500 500])
    % %     scatter(locs_ROI(:,locsColumns.xCol),locs_ROI(:,locsColumns.yCol),'.')
    % %     title('Selected ROI for DBSCAN analysis');
    % % 
    % %     fprintf('\n -- Plotted selected ROI  --\n')
    % 
    % 
    %     

    %     %% DBSCAN the data
    % 
    %     fprintf('DBSCAN analysis started');
    %     
    %     tic 
    % 
    %     % Normalizes localizations for DBSCAN analysis to 0
    %     dataDBS      = [];
    %     %dataDBS(:,1) = locs_ROI(:,locsColumns.xCol)-min(locs_ROI(:,locsColumns.xCol)); % x in mum
    %     dataDBS(:,1) = locs_Filtered(:,locsColumns.xCol)-min(locs_Filtered(:,locsColumns.xCol)); % x in nm  
    %     
    %     %dataDBS(:,2) = locs_ROI(:,locsColumns.yCol)-min(locs_ROI(:,locsColumns.yCol)); % y in mum
    %     dataDBS(:,2) = locs_Filtered(:,locsColumns.yCol)-min(locs_Filtered(:,locsColumns.yCol)); % y in nm 
    %     
    %     
    %     % Specifies input parameters for DBSCAN
    %     k_dbscan = 15;                                              % minimum number of neighbors within Eps
    %     Eps = 50;                                                   % maximum distance between points in order for them to be counted as neighbors, nm 
    % 
    %     [class,type]=DBSCAN(dataDBS,k_dbscan,Eps);                  % uses parameters specified at input
    %     class2=transpose(class);                                    % class - vector specifying assignment of the i-th point to certain cluster (m,1)
    %     type2=transpose(type);                                      % type - vector specifying type of the i-th point(core: 1, border: 0, outlier: -1)
    % 
    %     coreBorder = [];
    %     coreBorder = find(type2 >= 0);                              % gets the positions of all localisations either belonging to a cluster
    % 
    %     subset          = [];
    %     %subset          = locs_ROI(coreBorder,1:end); % contains all localisations that are either a core or a border point 
    %     subset          = locs_Filtered(coreBorder,1:end); % contains all localisations that are either a core or a border point 
    %     subset(:,end+1) = class2(coreBorder);         % adds for every localization belonging to a cluster to which cluster it belongs (cluster ID)  
    % 
    %     subsetP = [];                                 % contains for all localizations belonging to a cluster the x coordinate, y coordinate and cluster ID
    %     subsetP(:,1)    = dataDBS(coreBorder,1);      % x coordinate
    %     subsetP(:,2)    = dataDBS(coreBorder,2);      % y coordinate 
    %     subsetP(:,3)    = class2(coreBorder);         % cluster ID
    % 
    %     durationDBSCAN = (toc/60);
    %     %fprintf('\n -- DBSCAN finished in %f --\n',toc)
    %     display(['DBSCAN finished in ' round(num2str(toc/60)) ' min']);
    % 
    %     % Plots the filtered localisations
    %     figure('Position',[100 600 600 300]) 
    %     subplot(1,2,1)
    %     scatter(dataDBS(:,1),dataDBS(:,2),1);
    %     title('Filtered localizations before DBSCAN')
    %     axis([min(dataDBS(:,1)) max(dataDBS(:,1)) min(dataDBS(:,2)) max(dataDBS(:,2))])
    %     axis square
    %     xlabel('x [nm]');
    %     ylabel('y [nm]');
    %     box on
    % 
    %     % Plots only the localisations belonging to a cluster 
    %     subplot(1,2,2)
    %     scatter(subsetP(:,1),subsetP(:,2),1,mod(subsetP(:,3),30))
    %     title('Identified clusters by DBSCAN')
    %     axis on
    %     axis([min(dataDBS(:,1)) max(dataDBS(:,1)) min(dataDBS(:,2)) max(dataDBS(:,2))])
    %     xlabel('x [nm]');
    %     ylabel('y [nm]');
    %     axis square
    %     box on
    % 
    %     cd(savepath);
    %     savefig('Locs_vs_clusters');
    %     
    %     %% Filter cluster by length/number of localizations per cluster
    % 
    %     max_length = 5000; % long enough to exclude fiducials
    % 
    %     filtered_clusters = [];
    % 
    %     for i = 1:max(subsetP(:,3))             % Iterates over all clusters 
    % 
    %            target = find(subsetP(:,3)==i);  % Find all localisations belonging to the i-th cluster 
    % 
    %            cluster = [];
    %            cluster = subsetP(target,1:end); % Contains only the localisations belonging to the i-th cluster
    % 
    %        if size(target,1)<max_length==1  % If the number of localisations in a cluster is smaller than the maximum value given above
    % 
    %            filtered_clusters = vertcat(filtered_clusters, cluster); % Let this cluster remain 
    % 
    %        end
    % 
    %     end
    % 
    %     % Plots all identified clusters 
    %     figure('Position',[100 600 600 300]) % all data from both channels
    %     subplot(1,2,1)
    %     scatter(subsetP(:,1),subsetP(:,2),1,mod(subsetP(:,3),30))
    %     title('All identified clusters')
    %     axis on
    %     axis([min(dataDBS(:,1)) max(dataDBS(:,1)) min(dataDBS(:,2)) max(dataDBS(:,2))])
    %     xlabel('x [nm]');
    %     ylabel('y [nm]');
    %     axis square
    %     box on
    % 
    %     % Plots only the clusters below the threshold for the number of
    %     % localizations
    %     subplot(1,2,2)
    %     scatter(filtered_clusters(:,1),filtered_clusters(:,2),1,mod(filtered_clusters(:,3),30))
    %     title('Filtered clusters')
    %     axis on
    %     axis([min(dataDBS(:,1)) max(dataDBS(:,1)) min(dataDBS(:,2)) max(dataDBS(:,2))])
    %     xlabel('x [nm]');
    %     ylabel('y [nm]');
    %     axis square
    %     box on
    % 
    %     %% Save the image, clustered data
    % 
    %     pxlsize = 10.6;
    % 
    %     heigth  = round((max(subsetP(:,2))-min(subsetP(:,2)))/pxlsize);
    %     width   = round((max(subsetP(:,1))-min(subsetP(:,1)))/pxlsize);
    % 
    %     im = hist3([subsetP(:,2),subsetP(:,1)],[heigth width]);
    % 
    %     imG = imgaussfilt(im, pxlsize/10);
    % 
    %     figure
    %     imshow(imG,[0.01 1]);
    %     title('All identified clusters unfiltered gaussian blurred')
    %     colormap('hot');
    % 
    %     cd(savepath)
    % 
    %     I32 = [];
    %     I32 = uint32(im);
    % 
    %     %t = Tiff('clustered_EGFR_Ab.tiff','w');
    %     t = Tiff(strcat(savename, '.tiff'),'w');
    %     tagstruct.ImageLength     = size(I32,1);
    %     tagstruct.ImageWidth      = size(I32,2);
    %     tagstruct.Photometric     = Tiff.Photometric.MinIsBlack;
    %     tagstruct.BitsPerSample   = 32;
    %     tagstruct.SamplesPerPixel = 1;
    %     tagstruct.RowsPerStrip    = 16;
    %     tagstruct.PlanarConfiguration = Tiff.PlanarConfiguration.Chunky;
    %     tagstruct.Software        = 'MATLAB';
    %     t.setTag(tagstruct)
    % 
    %     t.write(I32);
    %     t.close()
    % 
    %     %% Save Clustering result
    % 
    %     % Saves localisations belonging to unfiltered clusters together with their cluster ID as a matlab variable
    %     cd(savepath);
    % 
    %     save([savename '.mat'],'subsetP');
    % 
    %     %% Cluster size
    % 
    %     all = []; clusterC = [];
    % 
    %     for i = 1:max(subsetP(:,3))             % Iterates over all clusters
    % 
    %        target = find(subsetP(:,3)==i);      % Find all localisations belonging to the i-th cluster             
    % 
    %        cluster = [];
    %        cluster = subsetP(target,1:2);       % Saves only the x and y coordinates of the localisations belonging to the i-th cluster
    % 
    %        clusterC(i,1) =  mean(cluster(:,1)); % Contains the mean x-coordinate for every cluster
    %        clusterC(i,2) =  mean(cluster(:,2)); % Contains the mean y-coordinate for every cluster
    % 
    %        k = convhull(cluster(:,1),cluster(:,2)); % Calculates the convex hull of the i-th cluster
    % 
    %         % Plots the i-th cluster together with its convex hull 
    %     %     figure
    %     %     plot(cluster(k,1),cluster(k,2),'r-',cluster(k,1),cluster(k,2),'b*');hold on;
    %     %     scatter(cluster(:,1),cluster(:,2));
    %     %     title(strcat (i, 'th clusters + convex hull');
    % 
    %        all(i,1) = polyarea(cluster(k,1),cluster(k,2));  % Computes the area of the convex hull of the i-th cluster [nm^2]
    %        all(i,2) = length(cluster);                      % Contains the number of localisations within the i-th cluster 
    %        all(i,3) = sqrt(polyarea(cluster(k,1),cluster(k,2))/pi); % "Radius" of the cluster 
    % 
    % 
    % 
    %     end
    % 
    %     %clc
    % 
    %     %MedianClusterArea = median(all(:,3))
    %     MedianClusterArea = median(all(:,1))
    % 
    %     % Clusters per area
    %     k = convhull(clusterC(:,1),clusterC(:,2));      % Calculates the convex hull of all clusters on the basis of the mean x- and y-coordinates of all clusters
    % 
    %     % Plots all unfiltered clusters including the convex hull
    %       figure
    %       plot(clusterC(k,1),clusterC(k,2),'r-',clusterC(k,1),clusterC(k,2),'b*');hold on;
    %       scatter(clusterC(:,1),clusterC(:,2));
    %       title('Mean positions of all unfiltered clusters + convex hull'); 
    % 
    %     ClustersPerArea = i/polyarea(clusterC(k,1),clusterC(k,2))       % Clusters per area [1/nm^2]
    % 
    %     ClustersPerArea = i/(polyarea(clusterC(k,1),clusterC(k,2))/1e6) % Clusters per area [1/�m^2]
    % 
    % 
    %     % Saves statistical data on clusters 
    %     cd(savepath);
    % 
    % %     figure; boxplot(all(:,1)); ylabel('Cluster area [nm^2]'); hold on; plot(1,all(:,1),'o'); title (filename); savefig('Cluster area');
    % %     figure; boxplot(all(:,2)); ylabel('Number of localisations per cluster'); hold on; plot(1,all(:,2),'o'); title (filename); savefig('Number of localisations');
    %     
    %     save('ClusterStatistics.mat', 'all');
    %     save('ClustersPerArea.mat', 'ClustersPerArea'); % in 1/�m^2
    %     save('k_dbscan.mat', 'k_dbscan');
    %     save('Eps.mat', 'Eps');
    %     save('DurationDBSCAN.mat', 'durationDBSCAN');
    %     save('DBSCAN_ROI.mat','all_rois_position');     % contains the indices of all localizations within the ROI (indices of the unfiltered localizations)
    % 
    %     fprintf('\n -- Cluster analysis completed  --\n')
    % 
    %     %% Two Step DBSCAN
    % 
    %     % %close all
    %     % 
    %     % tic 
    %     % dataDBS      = [];
    %     % dataDBS(:,1) = locs_Filtered(:,locsColumns.xCol); % x in mum
    %     % dataDBS(:,2) = locs_Filtered(:,locsColumns.yCol); % y in mum
    %     % 
    %     % % 1st DBSCAN
    %     % 
    %     % k   = 40;                                                    % minimum number of neighbors within Eps
    %     % Eps = 15;                                                    % minimum distance between points, nm
    %     % 
    %     % [class,type] = DBSCAN(dataDBS,k,Eps);                       % uses parameters specified at input
    %     % class2=transpose(class);                                    % class - vector specifying assignment of the i-th object to certain cluster (m,1)
    %     % type2=transpose(type);                                      % (core: 1, border: 0, outlier: -1)
    %     % 
    %     % coreBorder = [];
    %     % coreBorder = find(type2 >= 0);
    %     % 
    %     % notClustered = find(type2 == -1);
    %     % 
    %     % subset          = [];
    %     % subset          = locs_Filtered(coreBorder,1:end);
    %     % subset(:,end+1) = class2(coreBorder);
    %     % 
    %     % fprintf('\n -- DBSCAN finished in %f --\n',toc)
    %     % 
    %     % figure('Position',[100 600 600 300],'name','1st DBSCAN') % all data from both channels
    %     % subplot(1,2,1)
    %     % scatter(dataDBS(:,1),dataDBS(:,2),1);
    %     % axis([min(dataDBS(:,1)) max(dataDBS(:,1)) min(dataDBS(:,2)) max(dataDBS(:,2))])
    %     % box on
    %     % 
    %     % subplot(1,2,2)
    %     % scatter(subset(:,locsColumns.xCol),subset(:,locsColumns.yCol),1,mod(subset(:,end),10))
    %     % title('identified Clusters')
    %     % axis on
    %     % axis([min(dataDBS(:,1)) max(dataDBS(:,1)) min(dataDBS(:,2)) max(dataDBS(:,2))])
    %     % box on
    %     % % 
    %     % 
    %     % % 2nd DBSCAN
    %     % 
    %     % dataDBS      = [];
    %     % dataDBS = locs_Filtered(notClustered,1:end); % x in mum
    %     % 
    %     % 
    %     % k   = 30;                                                    % minimum number of neighbors within Eps
    %     % Eps = 20;                                                    % minimum distance between points, nm
    %     % 
    %     % [class,type] = DBSCAN(dataDBS(:,1:2),k,Eps);                       % uses parameters specified at input
    %     % class2=transpose(class);                                    % class - vector specifying assignment of the i-th object to certain cluster (m,1)
    %     % type2=transpose(type);                                      % (core: 1, border: 0, outlier: -1)
    %     % 
    %     % coreBorder = [];
    %     % coreBorder = find(type2 >= 0);
    %     % 
    %     % subset2          = [];
    %     % subset2          = dataDBS(coreBorder,1:end);
    %     % subset2(:,end+1) = class2(coreBorder);
    %     % 
    %     % fprintf('\n -- DBSCAN finished in %f --\n',toc)
    %     % 
    %     % figure('Position',[100 600 600 300],'name','2nd DBSCAN') % all data from both channels
    %     % subplot(1,2,1)
    %     % scatter(dataDBS(:,1),dataDBS(:,2),1);
    %     % axis([min(dataDBS(:,1)) max(dataDBS(:,1)) min(dataDBS(:,2)) max(dataDBS(:,2))])
    %     % box on
    %     % 
    %     % subplot(1,2,2)
    %     % scatter(subset2(:,locsColumns.xCol),subset2(:,locsColumns.yCol),1,mod(subset2(:,end),10))
    %     % title('identified Clusters')
    %     % axis on
    %     % axis([min(dataDBS(:,1)) max(dataDBS(:,1)) min(dataDBS(:,2)) max(dataDBS(:,2))])
    %     % box on
    %     % 
    %     % % Combine clusters
    %     % 
    %     % subset2(:,end) = subset2(:,end)+ max(subset(:,end));
    %     % all_clusters = [subset; subset2];
    %     % 
    %     % fprintf('\n -- DBSCAN finished in %f --\n',toc)
    %     % 
    %     % figure('Position',[100 600 600 300],'name','All identified Clusters') % all data from both channels
    %     % subplot(1,2,1)
    %     % scatter(locs_Filtered(:,locsColumns.xCol),locs_Filtered(:,locsColumns.yCol),1);
    %     % axis([min(locs_Filtered(:,locsColumns.xCol)) max(locs_Filtered(:,locsColumns.xCol)) min(locs_Filtered(:,locsColumns.yCol)) max(locs_Filtered(:,locsColumns.yCol))])
    %     % box on
    %     % 
    %     % subplot(1,2,2)
    %     % scatter(all_clusters(:,1),all_clusters(:,2),1,mod(all_clusters(:,end),10));
    %     % axis([min(locs_Filtered(:,locsColumns.xCol)) max(locs_Filtered(:,locsColumns.xCol)) min(locs_Filtered(:,locsColumns.yCol)) max(locs_Filtered(:,locsColumns.yCol))])
    %     % box on

    cd(defaultPath);
    save('Per_cell_batch_database_incl_results.mat','batch_database');
    %writetable(Results,'Per_cell_batch_results_201206_new.xlsx');
%         catch
%             % if an error occured during the analysis of a field of view the batch
%             % analysis is continued with the next field of view
%             cd(savepath);
%             fid = fopen('Analysis_error.txt','wt');
%             fprintf(fid, 'An error occured during the analysis of this field of view');
%             fclose(fid);                       
%             continue
%         end
    end

    %% Saves the batch database data    
    cd(defaultPath);
    %save('Per_cell_batch_database.mat','batch_database');
    %writetable(Results,'Per_cell_batch_results_new.xlsx');
    writetable(Results,'Per_cell_batch_results.xlsx');
    fprintf('\n -- Batch analysis completed  --\n')
    close all
   end