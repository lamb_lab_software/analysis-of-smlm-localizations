clear, clc, close all

cd('X:\Data\Inflammasome_Ivo_2015\STORM_experiments\dSTORM_Lausanne\Localisation_data\Matlabanalysisresults\AB+Fab');
addpath('X:\Data\Inflammasome_Ivo_2015\STORM software\RCC drift correction DBSCAN clustering Ivo');
Results = readtable('Percellbatchresultsallassingleset_wo_last_4.xlsx');
%Results = readtable('Per_cell_batch_results_all.xlsx');

number_of_data_sets = max(Results.data_set);

for current_data_set = 1:number_of_data_sets
    Indices_current_data_set                                                                  = find(Results.data_set(:,1) == current_data_set);
    data_sets.(strcat('Set_', num2str(current_data_set))).data                                = Results(Indices_current_data_set,:);
    data_sets.(strcat('Set_', num2str(current_data_set))).IndicesCellsWithSpeck               = find(data_sets.(strcat('Set_', num2str(current_data_set))).data.Cell_contains_speck(:,1) == true);
    data_sets.(strcat('Set_', num2str(current_data_set))).IndicesCompleteCellsWithSpeck       = find(data_sets.(strcat('Set_', num2str(current_data_set))).data.Cell_contains_speck(:,1) == true & data_sets.(strcat('Set_', num2str(current_data_set))).data.Cell_complete(:,1) == true);
    data_sets.(strcat('Set_', num2str(current_data_set))).IndicesCellsWithoutSpeck            = find(data_sets.(strcat('Set_', num2str(current_data_set))).data.Cell_contains_speck(:,1) == false);
    data_sets.(strcat('Set_', num2str(current_data_set))).IndicesCompleteCellsWithoutSpeck    = find(data_sets.(strcat('Set_', num2str(current_data_set))).data.Cell_contains_speck(:,1) == false & data_sets.(strcat('Set_', num2str(current_data_set))).data.Cell_complete(:,1) == true);
    data_sets.(strcat('Set_', num2str(current_data_set))).IndicesCompleteCells                = find(data_sets.(strcat('Set_', num2str(current_data_set))).data.Cell_complete(:,1) == true);
    data_sets.(strcat('Set_', num2str(current_data_set))).IndicesSpecks                       = find(data_sets.(strcat('Set_', num2str(current_data_set))).data.Cell_contains_speck(:,1) == true);
    data_sets.(strcat('Set_', num2str(current_data_set))).IndicesTimecourseSpecks             = find(data_sets.(strcat('Set_', num2str(current_data_set))).data.Timecourse(:,1) == true);
end
clear current_data_set


for current_data_set = 1:numel(fieldnames(data_sets)) % iterates over data sets    
    
    number_cells_current_data_set       = length(data_sets.(strcat('Set_', num2str(current_data_set))).data.Filepath_Locs(:,1));
    bgd_corrected_WF_int_entire_cell    = zeros(number_cells_current_data_set, 1);
    bgd_corrected_WF_int_cytoplasma     = zeros(number_cells_current_data_set, 1);
    bgd_corrected_WF_int_speck          = zeros(number_cells_current_data_set, 1);
    avg_bgd_pixel_intensity_all_FoVs    = zeros(number_cells_current_data_set, 1);
    
    for current_cell = 1:length(Indices_current_data_set(:,1)) % iterates over all cells of the current data set 
        path_folder_WF_current_cell                     = string(data_sets.(strcat('Set_', num2str(current_data_set))).data.Filepath_WF(current_cell,1));        
        filename_WF_current_cell                        = string(data_sets.(strcat('Set_', num2str(current_data_set))).data.Filename_WF(current_cell,1));
        filepath_WF_all_cells(current_cell,1)           = strcat(path_folder_WF_current_cell, filename_WF_current_cell);
    end
    clear current_cell    
    
    FoVs_current_data_set_wo_repetitions = unique(filepath_WF_all_cells(:,1), 'stable');     
    
    for current_FoV = 1:length(FoVs_current_data_set_wo_repetitions(:,1))    % iterates over all FoVs of the current data set    
        all_indices_current_FoV = find(filepath_WF_all_cells(:,1) == FoVs_current_data_set_wo_repetitions(current_FoV,1));
        first_index_current_FoV = all_indices_current_FoV(1,1);     
    
        path_folder_WF_current_FoV      = string(data_sets.(strcat('Set_', num2str(current_data_set))).data.Filepath_WF(first_index_current_FoV,1));        
        filename_WF_current_FoV         = string(data_sets.(strcat('Set_', num2str(current_data_set))).data.Filename_WF(first_index_current_FoV,1));
        filepath_WF_current_FoV         = strcat(path_folder_WF_current_FoV, filename_WF_current_FoV);
        current_image                   = imread(filepath_WF_current_FoV);
        size_WF_image                       = length(current_image(:,1));
        imshow(current_image,[0 500])
        Background_ROI                      = drawfreehand('LineWidth',2,'Color','r');                 % Creates the ROI selection tool        
        selected_bgd_area_mask              = uint16(poly2mask(Background_ROI.Position(:,1), Background_ROI.Position(:,2), size_WF_image, size_WF_image));
        close;
        clear Background_ROI
        cd(char(path_folder_WF_current_FoV));
        save('Bgd_selection_mask.mat','selected_bgd_area_mask');
        selected_bgd_area_size               = nnz(selected_bgd_area_mask); % selected area size in number of pixel
        selected_bgd_area                    = current_image.*selected_bgd_area_mask;
        integrated_intensity_bgd_area        = sum(selected_bgd_area, 'all');
        
        for current_index = 1:length(all_indices_current_FoV(:,1))
            avg_bgd_pixel_intensity_all_FoVs(all_indices_current_FoV(current_index,1),1) = integrated_intensity_bgd_area/selected_bgd_area_size;
        end        
                
     end   
    
    
    for current_cell = 1:length(Indices_current_data_set(:,1)) % iterates over all cells of the current data set 
        Cell_size_WF__nm2                   = data_sets.(strcat('Set_', num2str(current_data_set))).data.Cell_size_WF__um2(current_cell,1)*1000000;
        Cell_size_WF__pixels                = Cell_size_WF__nm2/(106^2);
        Bgd_WF_cell                         = Cell_size_WF__pixels*avg_bgd_pixel_intensity_all_FoVs(current_cell,1);
        bgd_corrected_WF_int_entire_cell(current_cell,1)    = data_sets.(strcat('Set_', num2str(current_data_set))).data.Int_intensity_WF_entire_cell__counts(current_cell,1)-Bgd_WF_cell;
        
        if data_sets.(strcat('Set_', num2str(current_data_set))).data.Cell_contains_speck(current_cell,1) == 1 % if the cell contains a speck 
            
            Speck_size_WF__nm2                  = data_sets.(strcat('Set_', num2str(current_data_set))).data.Speck_size_WF_manual__um2(current_cell,1)*1000000;
            Speck_size_WF__pixels               = Speck_size_WF__nm2/(106^2);
            Bgd_WF_speck                        = Speck_size_WF__pixels*avg_bgd_pixel_intensity_all_FoVs(current_cell,1);
            bgd_corrected_WF_int_speck(current_cell,1)          = data_sets.(strcat('Set_', num2str(current_data_set))).data.Int_intensity_WF_speck_manual__counts(current_cell,1)-Bgd_WF_speck;

            Cytoplasma_size_WF__nm2             = data_sets.(strcat('Set_', num2str(current_data_set))).data.Cytoplasma_size_WF_manual__um2(current_cell,1)*1000000;
            Cytoplasma_size_WF__pixels          = Cytoplasma_size_WF__nm2/(106^2);
            Bgd_WF_cytoplasma                   = Cytoplasma_size_WF__pixels*avg_bgd_pixel_intensity_all_FoVs(current_cell,1);
            bgd_corrected_WF_int_cytoplasma(current_cell,1)     = data_sets.(strcat('Set_', num2str(current_data_set))).data.Int_intensity_WF_cytoplasma_manual__counts(current_cell,1)-Bgd_WF_cytoplasma;
        
        end
        
    end
    
    Results.Bgd_cor_WF_int_entire_cell__counts(Indices_current_data_set,1) = bgd_corrected_WF_int_entire_cell(:,1);
    Results.Bgd_cor_WF_int_speck__counts(Indices_current_data_set,1) = bgd_corrected_WF_int_speck(:,1);
    Results.Bgd_cor_WF_int_cytoplasma__counts(Indices_current_data_set,1) = bgd_corrected_WF_int_cytoplasma(:,1);
    Results.Avg_Bgd_pixel_intensity__counts(Indices_current_data_set,1) = avg_bgd_pixel_intensity_all_FoVs(:,1);
    
end     

cd('X:\Data\Inflammasome_Ivo_2015\STORM_experiments\dSTORM_Lausanne\Localisation_data\Matlabanalysisresults\AB+Fab');
writetable(Results,'Percellbatchres_allsingleset_Bgdcor_all.xlsx');