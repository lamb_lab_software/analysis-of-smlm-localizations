function [locs_DC] = RCC_drift_correct_locs(locs_NDC, locsColumns, dimensionality_locs, localizer, pixel_size, image_size, DC_locs_save_path, DC_locs_file_name, RCC_path)

% Prepares data for RCC-based drift correction

%         fprintf('\n -- Preparing current FOV for RCC-based drift correction ... \n')   
% 
%         % Generate Coords variable as input for RCC
%         % Remove NaN and Inf
%         
%         switch Localizer 
%             case "Fang" 
%             coords(:,1) = (locs(:,locsColumns.xCol))/pixel_size; % coords in pixels
%             coords(:,2) = (locs(:,locsColumns.yCol))/pixel_size;
%         
%             case "The other one"
%             coords(:,1) = locs(:,locsColumns.xCol_px);
%             coords(:,2) = locs(:,locsColumns.yCol_px);
%         end
            
%         coords(:,3) = locs(:,locsColumns.frameCol);

        NDC_coords(:,1) = locs_NDC(:,locsColumns.xCol)/pixel_size;
        NDC_coords(:,2) = locs_NDC(:,locsColumns.yCol)/pixel_size;
        NDC_coords(:,3) = locs_NDC(:,locsColumns.frameCol);

        % Remove NaN and Inf
        temp    = NDC_coords;
        clear NDC_coords
        NDC_coords  = temp( ~any( isnan( temp(:,1) ) | isinf( temp(:,1) ), 2 ),: );  % This variable contains the x and y coordinates and the corresponding frame 

        fprintf('\n -- Ready for drift correction -- \n')


        %% In case the correction should only run on an ROI of the locs  
        %Select region to correct

    %     pxlsize = 1;
    % 
    %     heigth  = round((max(coords(:,2))-min(coords(:,2)))/pxlsize);
    %     width   = round((max(coords(:,1))-min(coords(:,1)))/pxlsize);
    %     im      = hist3([coords(:,1),coords(:,2)],[width heigth]); % heigth x width
    % 
    %     % Select rectangles
    % 
    %     rect = []; 
    % 
    %     figure('Position',[100 200 600 600])
    %     f = imagesc(im, [0 10]);
    %     %f = imagesc(imrotate(im,90), [0 10]);
    %     %f = imagesc(imrotate(im,90),[0 2e3]);
    %     %f = imagesc(flipdim(imrotate(im,-90),2) ,[0 10]);
    %     colormap('parula'); colorbar;
    %     title('Select ROI for RCC-based drift correction')
    % 
    %     rect = getrect;
    % 
    %     fprintf('\n -- ROI selected --\n')
    % 
    %     xmin = min(coords(:,1))+ rect(1,1)*pxlsize;
    %     ymin = max(coords(:,2))- rect(1,2)*pxlsize - (rect(1,4)*pxlsize) ;
    %     xmax = xmin + 512;
    %     ymax = ymin + 512;
    % 
    %     target      = find(coords(:,1)>xmin & coords(:,1)<xmax & coords(:,2)>ymin & coords(:,2)<ymax);
    %     coords_ROI  = coords(target,1:end);
    % 
    %     % Show cropped region
    % 
    %     heigth  = round((max(coords_ROI(:,2))-min(coords_ROI(:,2)))/pxlsize);
    %     width   = round((max(coords_ROI(:,1))-min(coords_ROI(:,1)))/pxlsize);
    %     im      = hist3([coords_ROI(:,1),coords_ROI(:,2)],[width heigth]); % heigth x width
    % 
    %     figure('Position',[100 200 600 600])
    %     imagesc(im, [0 10]);
    %     %imagesc(imrotate(im,90), [0 10]);
    %     %imagesc(imrotate(im,90),[0 2e3]);
    %     %imagesc(flipdim(imrotate(im,-90),2) ,[0 10]);
    %     colormap('parula'); colorbar;
    %     title('Selected ROI for RCC-based drift correction')
    % 
    %     coords_ROI(:,1) = coords_ROI(:,1)-min(coords_ROI(:,1));
    %     coords_ROI(:,2) = coords_ROI(:,2)-min(coords_ROI(:,2));

        %% Drift correct
        tic

        cd(RCC_path);
        %cd([fitting_dist '\RCC_drift_correct']);                                % Changes to the directory in which the code for RCC-based drift correction is saved

        % Input:    NDS_coords:         localization coordinates [x y frame], 
        %           segpara:            segmentation parameters (time wimdow, frame)
        %           imsize:             image size (pixel)
        %           pixelsize:          camera pixel size (nm)
        %           binsize:            spatial bin pixel size (nm)
        %           rmax:               error threshold for re-calculate the drift (pixel)
        % Output:   coordscorr:         localization coordinates after correction [xc yc] 
        %           finaldrift:         drift curve (save A and b matrix for other analysis)

        %finaldrift = RCC_TS(filepath, 1000, 256, 160, 30, 0.2);

        segpara     = max(NDC_coords(:,3))/5;                                 % the stack of all recorded frames is devided into 5 parts, which are each combined into one segment. The cross-correlation is calculated between the segments, the obtained drift between the segments is interpolated -> one drift value for each frame
        imsize      = image_size;                                               % the value entered in the beginning of the script
        %pixel_size 	= 106;
        binsize     = 30;

        [coordscorr, finaldrift] = RCC(NDC_coords, segpara, imsize, pixel_size, binsize, 0.2);       % isn't here "corrdscorr" already what we need? Compare it with coordsDC
        %[coordscorr, finaldrift] = RCC(coords_ROI, segpara, imsize, pixelsize, binsize, 0.2);
        %[coordscorr, finaldrift] = DCC(coords, segpara, imsize, pixelsize, binsize);

        display(['DC finished in ' round(num2str(toc/60)) ' min']);

        %% Plot Drift curves

        figure('Position',[100 100 900 400]);                                    % what are the units here? Can only be nm or pixels 
        subplot(2,1,1);
        plot(finaldrift(:,1));
        xlabel('Movie frame');
        ylabel('Drift [pixels]');
        title('x Drift');

        subplot(2,1,2);
        plot(finaldrift(:,2));
        xlabel('Movie frame');
        ylabel('Drift [pixels]');
        title('y Drift');

        cd(DC_locs_save_path);                                                           % saves drift curves to the saving directory of the current field of view
        savefig('Drift_curves');
        close;        
        
        
        

        %% Apply correction to coords WOULD NOT BE NECESSARY AS THE RCC CODE ALREADY GIVES THE DRIFT-CORRECTED COORDINATES BUT THE RESULTS ARE THE SAME

%         deltaXY      = [];                          
%         deltaXY(:,1) = NDS_coords(:,3);                 % frames of all localisations
%         deltaXY(:,2) = finaldrift(deltaXY(:,1),1);      % x drift for all localisations (since the drift is calculated per frame the drift will be the same for the localisations in the same frame)
%         deltaXY(:,3) = finaldrift(deltaXY(:,1),2);      % y drift for all localisations (since the drift is calculated per frame the drift will be the same for the localisations in the same frame)
% 
%         coordsDC      = [];
%         coordsDC(:,1) = NDS_coords(:,1)-deltaXY(:,2);   % x coordinates - x drift (if the sample drifts in the positive direction the drift needs to be subtracted to reach the correct coordinate; if the sample drifts in the negative direction (negative value) the amount of drift needs to be added -> subtracting negative value leads to addition) 
%         coordsDC(:,2) = NDS_coords(:,2)-deltaXY(:,3);   % y coordinates - y drift
% 
%         display('Coord corrected');
% 
%         clear coords
% 
%         locs_DC                         = locs;                             % locs_DC contains all localisations with their drift-corrected coordinates
%         locs_DC(:,locsColumns.xCol_px)  = coordsDC(:,1);                    % x [pixels]
%         locs_DC(:,locsColumns.yCol_px)  = coordsDC(:,2);                    % y [pixels]
%         locs_DC(:,locsColumns.xCol)     = coordsDC(:,1)*pixel_size;         % x [nm]
%         locs_DC(:,locsColumns.yCol)     = coordsDC(:,2)*pixel_size;         % y [nm]


        DC_x_coords_px = coordscorr(:,1);
        DC_y_coords_px = coordscorr(:,2);
        
        locs_DC                         = locs_NDC;
        locs_DC(:,locsColumns.xCol_px)  = DC_x_coords_px;               % drift-corrected x-coordinates in pixels 
        locs_DC(:,locsColumns.yCol_px)  = DC_y_coords_px;               % drift-corrected y-coordinates in pixels
        locs_DC(:,locsColumns.xCol)     = DC_x_coords_px*pixel_size;    % drift-corrected x-coordinates in nm
        locs_DC(:,locsColumns.yCol)     = DC_y_coords_px*pixel_size;    % drift-corrected y-coordinates in nm

        %% Plot corrected localisations

        % Plot localisations ...  
        figure('Position',[300 400 800 300])
        subplot(1,2,1);                                     % ... before drift correction 
        scatter(locs_NDC(:,locsColumns.xCol),locs_NDC(:,locsColumns.yCol),2);
        title('Before drift correction');
        xlabel('x [nm]');
        ylabel('y [nm]');
        axis square                      
        xlim([-5000 image_size*pixel_size+5000]); 
        ylim([-5000 image_size*pixel_size+5000]);
                
        subplot(1,2,2)                                      % ... after drift correction
        scatter(locs_DC(:,locsColumns.xCol),locs_DC(:,locsColumns.yCol),2);
        title('After drift correction');
        xlabel('x [nm]');
        ylabel('y [nm]');
        axis square
        xlim([-5000 image_size*pixel_size+5000]); 
        ylim([-5000 image_size*pixel_size+5000]);
        
        cd(DC_locs_save_path);
        gcf; savefig('Uncorrected vs. drift-corrected locs.fig'); % Save figure
        close;

        %% Save corrected localisations   
     
        
        
        
        % Save file
        cd(DC_locs_save_path);
        %NameCorrected = [filename '_DC_forTS.csv'];
        fileID = fopen(strcat(DC_locs_file_name,'.csv'),'w');

        switch  dimensionality_locs 
            
            case "2D"
                
                switch localizer
                    case "Fang"                      
                        fprintf(fileID,[['x [nm],y [nm],frame,uncertainty [nm],intensity [photon],offset [photon],loglikelihood,sigma [nm],x [pixels],y [pixels]'] ' \n']);
                    case "The other one"
                        fprintf(fileID,[['frame,x_pix,y_pix,sx_pix,sy_pix,photons,background,crlb_x,crlb_y,crlb_photons,crlb_background,logLikelyhood,x_nm,y_nm,crlb_xnm,crlb_ynm'] ' \n']);                     
               end          
                
%             case "3D" 
%                 
%                 switch localizer 
%                     case "Fang"
%                         fprintf(fileID,[['"x [nm]","y [nm]","frame","uncertainty [nm]","intensity [photon]","offset [photon]","loglikelihood","sigma [nm]","x [pixels]","y [pixels]"'] ' \n']);
%                     case "The other one"
%                         fprintf(fileID,[['"frame","x_pix","y_pix","sx_pix","sy_pix",","photons","background","crlb_x","crlb_y","crlb_photons","crlb_background","logLikelihood","x_nm","y_nm","crlb_xnm","crlb_ynm"'] ' \n']);
%                 end 

        end     

        dlmwrite(strcat(DC_locs_file_name,'.csv'),locs_DC,'-append');
        fclose('all');
