function Render_SR_image(x_locs, y_locs, pixel_size_SR_image, SR_image_name, savepath)

% Calculates the height and width of the SR image
heigth  = round((max(y_locs)-min(y_locs))/pixel_size_SR_image);
width   = round((max(x_locs)-min(x_locs))/pixel_size_SR_image);

% Generates a 3D histogram out of the localisations
im = hist3([y_locs,x_locs],[heigth width]);
range = getrangefromclass(uint32(1));
im32 = uint32( im*(range(2)-range(1)) + range(1) );
im32_single = single(im32);
%im32 = uint32(im);
%im32_single = single(im32);
% filters the image with a 2-D Gaussian smoothing kernel with standard deviation of the pixel size devided by 5
imG = imgaussfilt(im, pixel_size_SR_image/10);
figure; imshow(imG);
close;
%imnorm = (imG-min(imG(:)))./(max(imG(:))-min(imG(:)));
%figure; imshow(imnorm, []); title('Normalized double precision');
range = getrangefromclass(uint32(1));
imG32 = uint32( imG*(range(2)-range(1)) + range(1) );
% figure; imshow(I32); title('Normalized uint32 precision');
% figure; imshow(I32); colormap('hot'); title('Normalized uint32 precision hot');

% figure
% imshow(imG,[0.01 1]);
% title(SR_image_name)
% colormap('hot');

imG32_single=single(imG32);

addpath('X:\Data\Inflammasome_Ivo_2015\STORM software\RCC drift correction DBSCAN clustering Ivo');


saveastiff(imG32_single,strcat(savepath, '\', SR_image_name, '.tiff')); %Saves the image Gaussian blurred

SR_image_name_wo = strcat(SR_image_name, 'wob');

saveastiff(im32_single,strcat(savepath, '\', SR_image_name_wo, '.tiff')); %Saves the image wo Gaussian blur









%cd(savepath)

% I32 = [];
% I32 = uint32(imG);
%I32 = uint32(im);

%t = Tiff('Filt_DC_locs.tiff','w');
% t = Tiff(strcat(SR_image_name, '.tiff'),'w');                               % Opens a TIFF file with the specified name for writting; discards existing content
% tagstruct.ImageLength     = size(I32,1);
% tagstruct.ImageWidth      = size(I32,2);
% tagstruct.Photometric     = Tiff.Photometric.MinIsBlack;
% tagstruct.BitsPerSample   = 32;
% tagstruct.SamplesPerPixel = 1;
% tagstruct.RowsPerStrip    = 16;
% tagstruct.PlanarConfiguration = Tiff.PlanarConfiguration.Chunky;
% tagstruct.Software        = 'MATLAB';
% t.setTag(tagstruct)
% %t.setTag('SampleFormat', Tiff.SampleFormat.Int);
% 
% t.write(I32);
% t.close()