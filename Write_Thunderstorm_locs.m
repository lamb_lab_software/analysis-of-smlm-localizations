function Write_Thunderstorm_locs(original_locs, locsColumns, dimensionality_locs, localizer, TS_locs_file_name, savepath)

    locsTS = [];

    switch  dimensionality_locs
        
        case "2D"
            
            switch localizer
                
                case "Fang"
                    
                    locsTS(:,1) = original_locs(:,locsColumns.frameCol);        % frame
                    locsTS(:,2) = original_locs(:,locsColumns.xCol);            % x nm
                    locsTS(:,3) = original_locs(:,locsColumns.yCol);            % y nm
                    locsTS(:,4) = original_locs(:,locsColumns.intensityCol);    % intensity [photons]
                    locsTS(:,5) = original_locs(:,locsColumns.LLCol);           % LL     
                    
                case "The other one"
                    
                    locsTS(:,1) = original_locs(:,locsColumns.frameCol);        % frame
                    locsTS(:,2) = original_locs(:,locsColumns.xCol);            % x nm
                    locsTS(:,3) = original_locs(:,locsColumns.yCol);            % y nm
                    locsTS(:,4) = original_locs(:,locsColumns.photonsCol);      % intensity [photons]
                    locsTS(:,5) = original_locs(:,locsColumns.LLCol);           % LL     
                    
            end

        case "3D"
            
            switch localizer
                
                case "Fang"
            
                    locsTS(:,1) = original_locs(:,locsColumns.frameCol);        % frames
                    locsTS(:,2) = original_locs(:,locsColumns.xCol);            % x nm
                    locsTS(:,3) = original_locs(:,locsColumns.yCol);            % y nm
                    locsTS(:,4) = original_locs(:,locsColumns.zCol);            % z nm
                    locsTS(:,5) = original_locs(:,locsColumns.intensityCol);    % intensity [photons]
                    locsTS(:,6) = original_locs(:,locsColumns.LLCol);           % LL    
            
                case "The other one"
                    
                    locsTS(:,1) = original_locs(:,locsColumns.frameCol);        % frames
                    locsTS(:,2) = original_locs(:,locsColumns.xCol);            % x nm
                    locsTS(:,3) = original_locs(:,locsColumns.yCol);            % y nm
                    locsTS(:,4) = original_locs(:,locsColumns.zCol);            % z nm
                    locsTS(:,5) = original_locs(:,locsColumns.photonsCol);      % intensity [photons]
                    locsTS(:,6) = original_locs(:,locsColumns.LLCol);           % LL  
                    
            end
    end

    % Save file
    cd(savepath);
    %NameCorrected = [filename '_DC_forTS.csv'];
    fileID = fopen(TS_locs_file_name,'w');

    switch  dimensionality_locs 
        case "2D" 
            fprintf(fileID,[['"frame","x [nm]","y [nm]","intensity [photons]","loglikelihood"'] ' \n']);
        case "3D"  
            fprintf(fileID,[['"frame","x [nm]","y [nm]","z [nm]","intensity [photons]","loglikelihood"'] ' \n']);
    end     

    dlmwrite(TS_locs_file_name,locsTS,'-append');
    fclose('all');

end

