function [filtered_localisations] = filter_localisations(unfiltered_localisations, used_localiser, locsColumns, save_filter, savepath)

switch  used_localiser 
    case "Fang"           
        %% Choose filter parameters                  
        %minFrame            = 1;
        minFrame            = 100;
        maxFrame            = max(unfiltered_localisations(:,locsColumns.frameCol));
        %minIntensity        = 0;        
        minIntensity        = 600;
        maxIntensity        = 3500;
        %maxIntensity        = 5500;
        maxLL               = 600;
        %maxLL               = 1250;
        %maxLL               = 250;
        %AF647 Fab maxLL               = 500;
        %AF647 nanobody: maxLL               = 1250;
        %minZ                = -1000;
        %maxZ                = 1000;
        %AF647 nanobody: maxUncert           = 70;
        maxUncert           = 25;
        %maxUncert           = 50;
        %AF647 nanobody: maxSigma             = 400;
        %maxSigma            = 350;
        maxSigma             = 200;
        %maxSigma = 250;

        % Saves filter in variable 
        loc_filter.minFrame     = minFrame;
        loc_filter.maxFrame     = maxFrame;
        loc_filter.minPhotons   = minIntensity;
        loc_filter.maxPhotons   = maxIntensity;
        loc_filter.maxLL        = maxLL;
        %loc_filter.minZ         = minZ;
        %loc_filter.maxZ         = maxZ;
        loc_filter.maxUncert    = maxUncert;
        loc_filter.maxSigma     = maxSigma;
        
        % Filter the localisations given to the function 
        filter                                  = [];    
        filter                                  = find(unfiltered_localisations(:,locsColumns.intensityCol) > minIntensity & unfiltered_localisations(:,locsColumns.intensityCol) < maxIntensity ... 
                                                  & unfiltered_localisations(:,locsColumns.frameCol) > minFrame & unfiltered_localisations(:,locsColumns.frameCol) < maxFrame & unfiltered_localisations(:,locsColumns.sigmaCol) < maxSigma ...
                                                  & unfiltered_localisations(:,locsColumns.uncertCol) < maxUncert & unfiltered_localisations(:,locsColumns.LLCol) < maxLL); 
                                                  %& unfiltered_localisations(:,locsColumns.zCol) > minZ & unfiltered_localisations(:,locsColumns.zCol) < maxZ);
                                                  
        
        filtered_localisations                  = unfiltered_localisations(filter,1:end);    
      

    case "The other one"
        % minFrame            = 100;
        % minPhotons          = 400;
        % Maxuncertainty      = 20;
        % logLikelihood       = 300;

        minFrame            = 100;
        maxFrame            = max(unfiltered_localisations(:,locsColumns.frameCol));
        minPhotons          = 400;
        maxPhotons          = 5000;
        minLL               = -450;
        %minZ                = -1000;
        %maxZ                = 1000;
        maxBG               = 7; 
        %maxBG               = 4;

        % Saves filter in variable 
        loc_filter.minFrame     = minFrame;
        loc_filter.maxFrame     = maxFrame;
        loc_filter.minPhotons   = minPhotons;
        loc_filter.maxPhotons   = maxPhotons;
        loc_filter.minLL        = minLL;
        %loc_filter.minZ         = minZ;
        %loc_filter.maxZ         = maxZ;
        loc_filter.maxBG        = maxBG; 
        
        % Filter the localisations given to the function 
        filter                                  = [];    
        filter                                  = find(unfiltered_localisations(:,locsColumns.photonsCol) > minPhotons & unfiltered_localisations(:,locsColumns.photonsCol) < maxPhotons & unfiltered_localisations(:,locsColumns.LLCol) > minLL ... 
                                                  & unfiltered_localisations(:,locsColumns.frameCol) > minFrame & unfiltered_localisations(:,locsColumns.frameCol) < maxFrame &  ... 
                                                  unfiltered_localisations(:,locsColumns.bgCol) < maxBG);
                                                  %unfiltered_localisations(:,locsColumns.zCol) > minZ & unfiltered_localisations(:,locsColumns.zCol) < maxZ
                                                      
        filtered_localisations                  = unfiltered_localisations(filter,1:end);       
end


% Saves the filter if user wants that       
switch save_filter
    case "Yes"
        cd(savepath);
        save('LocalisationFilter.mat', 'loc_filter');
    case "No"        
end
        
        
        
        
        
        
        
        
        
        
        
        
 