clear
close all
clc
cd('X:\Data\Inflammasome_Ivo_2015\STORM_experiments\dSTORM_Lausanne\Localisation_data\Matlabanalysisresults\AB+Fab');
%cd('X:\Data\Inflammasome_Ivo_2015\STORM_experiments\dSTORM_Lausanne\Localisation_data\Matlabanalysisresults\DL755 NB');

addpath('X:\Data\Inflammasome_Ivo_2015\STORM software\RCC drift correction DBSCAN clustering Ivo');
addpath('X:\Data\Inflammasome_Ivo_2015\STORM software\RCC drift correction DBSCAN clustering Ivo\Density_codes');
addpath('X:\Users\Ivo\Software\Distribute figures');

savepath = 'X:\Data\Inflammasome_Ivo_2015\STORM_experiments\dSTORM_Lausanne\Localisation_data\Matlabanalysisresults\AB+Fab\Figure5\Sorted_speck_montage\Speck_density\';

%Results = readtable('Per_cell_batch_results_all_DL755_as_single_set.xlsx');
%Results = readtable('Per_cell_batch_results_all_as_single_set.xlsx');
%Results = readtable('Per_cell_batch_results_all_as_single_set_incl._TCmat.xlsx');
Results = readtable('Per_cell_batch_results_all_as_single_set.xlsx');

number_of_data_sets = max(Results.data_set);





for current_data_set = 1:number_of_data_sets
    Indices_current_data_set                                                                  = find(Results.data_set(:,1) == current_data_set);
    data_sets.(strcat('Set_', num2str(current_data_set))).data                                = Results(Indices_current_data_set,:);
    data_sets.(strcat('Set_', num2str(current_data_set))).IndicesCellsWithSpeck               = find(data_sets.(strcat('Set_', num2str(current_data_set))).data.Cell_contains_speck(:,1) == true);
    data_sets.(strcat('Set_', num2str(current_data_set))).IndicesCompleteCellsWithSpeck       = find(data_sets.(strcat('Set_', num2str(current_data_set))).data.Cell_contains_speck(:,1) == true & data_sets.(strcat('Set_', num2str(current_data_set))).data.Cell_complete(:,1) == true);
    data_sets.(strcat('Set_', num2str(current_data_set))).IndicesCellsWithoutSpeck            = find(data_sets.(strcat('Set_', num2str(current_data_set))).data.Cell_contains_speck(:,1) == false);
    data_sets.(strcat('Set_', num2str(current_data_set))).IndicesCompleteCellsWithoutSpeck    = find(data_sets.(strcat('Set_', num2str(current_data_set))).data.Cell_contains_speck(:,1) == false & data_sets.(strcat('Set_', num2str(current_data_set))).data.Cell_complete(:,1) == true);
    data_sets.(strcat('Set_', num2str(current_data_set))).IndicesCompleteCells                = find(data_sets.(strcat('Set_', num2str(current_data_set))).data.Cell_complete(:,1) == true);
    data_sets.(strcat('Set_', num2str(current_data_set))).IndicesTimecourseSpecks             = find(data_sets.(strcat('Set_', num2str(current_data_set))).data.Timecourse(:,1) == true);
    data_sets.(strcat('Set_', num2str(current_data_set))).IndicesFilamentsMeasured            = find(data_sets.(strcat('Set_', num2str(current_data_set))).data.Filaments_measured(:,1) == true);
end

figure('name', 'Gauss1');
%figure('name', 'Gauss2');
%figure('name', 'Gauss3');
width_filament_all = [];
distance_filaments_all = [];


for current_speck = 1:length(data_sets.(strcat('Set_', num2str(current_data_set))).IndicesFilamentsMeasured(:,1))
    
    current_filename_locs = data_sets.(strcat('Set_', num2str(current_data_set))).data.Filename_Locs(data_sets.(strcat('Set_', num2str(current_data_set))).IndicesFilamentsMeasured(current_speck),1);
       current_filename_locs_string = char(current_filename_locs);
       index_M = strfind(current_filename_locs_string, 'MM');
       if isempty(index_M)
           Localizer = "Too";
           index_M = strfind(current_filename_locs_string, 'locs'); %Alternative for "The other localizer"
       else 
           Localizer = "Fang";
       end               
       current_sample_name = current_filename_locs_string(1:(index_M-2));       
       %current_cell_number = char(data_sets.(strcat('Set_', num2str(current_data_set))).data.Cell_number(data_sets.(strcat('Set_', num2str(current_data_set))).IndicesFilamentsMeasured(current_speck),1));       
       current_cell_filaments_folder = char(strcat(data_sets.(strcat('Set_', num2str(current_data_set))).data.Filepath_Locs(data_sets.(strcat('Set_', num2str(current_data_set))).IndicesFilamentsMeasured(current_speck),1), 'PerCell_', current_sample_name, '/Filament_diameter'));

       % Get a list of all files in the folder with the desired file name pattern.
        filePattern = fullfile(current_cell_filaments_folder, '*.csv'); % Change to whatever pattern you need.
        theFiles = dir(filePattern);
       
       for k = 1 : length(theFiles)
            baseFileName = theFiles(k).name;
            fullFileName = fullfile(theFiles(k).folder, baseFileName);
            
            if string(fullFileName) == "X:\Data\Inflammasome_Ivo_2015\STORM_experiments\dSTORM_Lausanne\Localisation_data\4th_data_set_02a03_03_2020_Ivos6thexp\2020-03-03_Controls_2D\locResults_Fang_Thr100\Sample3_12_1\PerCell_Sample3_12_1\Filament_diameter\Values_ROI1.csv" ...
               || string(fullFileName) == "X:\Data\Inflammasome_Ivo_2015\STORM_experiments\dSTORM_Lausanne\Localisation_data\7thdataset02_07u04072020\200702Inflamm2C\locResults\AF647FangThresh100\Sample4_LPSNig_8_8\PerCell_Sample4_LPSNig_8_8\Filament_diameter\Values_ROI1.csv"
            continue
            end
            values_current_ROI = readtable(fullFileName);
            
            % X data 
            x = values_current_ROI.Distance___m_(:,1);

            % Y data
            y = values_current_ROI.Gray_Value(:,1);

            y = y - min(y);

            % Resamples X coordinates 
            x2 = min(x):0.0001:max(x);

            
            
            % Fits the data 
            [f, gof] = fit(x,y,'gauss1');
            fitting_model = 'gauss1';           
            
            if gof.rsquare < 0.9
                
                [f, gof] = fit(x,y,'gauss2');
                fitting_model = 'gauss2';
                
                if gof.rsquare < 0.9
                    
                    [f, gof] = fit(x,y,'gauss3');
                    fitting_model = 'gauss3';
                    
                end 
                
            end

            
            
            switch fitting_model
            
                case 'gauss1'
                    
                    fh = findobj( 'Type', 'Figure', 'Name', 'Gauss1' );
                    figure(fh);
                    plot(x,y,'g'); hold on;
                    plot(f,x,y); 
                    xlabel('Position (�m)');
                    ylabel('Counts');
                    legend(gca,'off');
                    
                    % Retrieves the coefficients of the 2 dimensional gaussian
                    a1=f.a1;
                    b1=f.b1;
                    c1=f.c1;    

                    % Calculates the Y coordinates of the first Gaussian component 
                    for i=1:length(x2)
                        gauss1(i)=a1*exp(-((x2(i)-b1)/c1)^2);
                    end             

                    % Calculates the width of the peak
                    filament_start_index = find(gauss1>=(max(gauss1)/2), 1, 'first');
                    filament_start_value = x2(1,filament_start_index);
                    filament_end_index = find(gauss1>=(max(gauss1)/2), 1, 'last');
                    filament_end_value = x2(1,filament_end_index);
                    width_filament = filament_end_value - filament_start_value;
                                        
                    width_filament_all(length(width_filament_all)+1, 1) = width_filament;
                    clear gauss1
                    
                    
                        
                case 'gauss2'
                    
%                     fh = findobj( 'Type', 'Figure', 'Name', 'Gauss2' );
%                     figure(fh);
%                     plot(f,x,y); hold on;
                    current_figure = figure; plot(f,x,y); hold on;
                    xlabel('Position (�m)');
                    ylabel('Counts');
                    legend(gca,'off');
                    
                    % Retrieves the coefficients of the 2 dimensional gaussian
                    a1=f.a1;
                    b1=f.b1;
                    c1=f.c1;
                    
                    % Calculates the Y coordinates of the first Gaussian component 
                    for i=1:length(x2)
                        gauss1(i)=a1*exp(-((x2(i)-b1)/c1)^2);
                    end
                    
                    plot(x2, gauss1, 'm');
                    
                    % Calculates the width of the peak
                    filament_start_index = find(gauss1>=(max(gauss1)/2), 1, 'first');
                    filament_start_value = x2(1,filament_start_index);
                    filament_end_index = find(gauss1>=(max(gauss1)/2), 1, 'last');
                    filament_end_value = x2(1,filament_end_index);
                    width_filament = filament_end_value - filament_start_value;
                    
                    if ~isempty(width_filament) 
                    
                    width_filament_all(length(width_filament_all)+1, 1) = width_filament;
                    
                    end
                    
                    
                    % Retrieves the coefficients of the 2 dimensional gaussian
                    a2=f.a2;
                    b2=f.b2;
                    c2=f.c2;
                    
                    % Calculates the Y coordinates of the first Gaussian component 
                    for i=1:length(x2)
                        gauss2(i)=a2*exp(-((x2(i)-b2)/c2)^2);
                    end
                    
                    plot(x2, gauss2, 'c');
                    
                    % Calculates the width of the peak
                    filament_start_index = find(gauss2>=(max(gauss2)/2), 1, 'first');
                    filament_start_value = x2(1,filament_start_index);
                    filament_end_index = find(gauss2>=(max(gauss2)/2), 1, 'last');
                    filament_end_value = x2(1,filament_end_index);
                    width_filament = filament_end_value - filament_start_value;
                    
                    if ~isempty(width_filament) 
                    
                    width_filament_all(length(width_filament_all)+1, 1) = width_filament;
                    
                    end
                    
                    gauss3 = gauss1 + gauss2;
                    plot(x2, gauss3, 'g');
                    
                    if ~isequal(max(gauss1), 0)
                    filament_1_max_index = find(gauss1 == max(gauss1));
                    filament_1_max_location = x2(1,filament_1_max_index);
                    end
                    
                    if ~isequal(max(gauss2), 0)
                    filament_2_max_index = find(gauss2 == max(gauss2));
                    filament_2_max_location = x2(1,filament_2_max_index);
                    end
                    
                    if exist('filament_1_max_location','var') == 1 && exist('filament_2_max_location','var') == 1 
                    distance_filaments = abs(filament_1_max_location - filament_2_max_location);
                    distance_filaments_all(length(distance_filaments_all)+1, 1) = distance_filaments;
                    title(['Peak-to-peak distance: ', num2str(distance_filaments)]);
                    end
                    
                    clear gauss1 gauss2 gauss3 filament_1_max_location filament_2_max_location                    
                    
                    
                    
                case 'gauss3'
                    
%                     fh = findobj( 'Type', 'Figure', 'Name', 'Gauss3' );
%                     figure(fh);
%                     plot(f,x,y); hold on;
                    current_figure = figure; plot(f,x,y); hold on;
                    xlabel('Position (�m)');
                    ylabel('Counts');
                    legend(gca,'off');
                    
                    % Retrieves the coefficients of the 2 dimensional gaussian
                    a1=f.a1;
                    b1=f.b1;
                    c1=f.c1;
                    
                    % Calculates the Y coordinates of the first Gaussian component 
                    for i=1:length(x2)
                        gauss1(i)=a1*exp(-((x2(i)-b1)/c1)^2);
                    end
                    
                    plot(x2, gauss1, 'm');
                    
                    % Calculates the width of the peak
                    filament_start_index = find(gauss1>=(max(gauss1)/2), 1, 'first');
                    filament_start_value = x2(1,filament_start_index);
                    filament_end_index = find(gauss1>=(max(gauss1)/2), 1, 'last');
                    filament_end_value = x2(1,filament_end_index);
                    width_filament = filament_end_value - filament_start_value;
                    
                    width_filament_all(length(width_filament_all)+1, 1) = width_filament;
                    
                    
                    % Retrieves the coefficients of the 2 dimensional gaussian
                    a2=f.a2;
                    b2=f.b2;
                    c2=f.c2;
                    
                    % Calculates the Y coordinates of the first Gaussian component 
                    for i=1:length(x2)
                        gauss2(i)=a2*exp(-((x2(i)-b2)/c2)^2);
                    end                                
            
                    plot(x2, gauss2, 'c');
                    
                    % Calculates the width of the peak
                    filament_start_index = find(gauss2>=(max(gauss2)/2), 1, 'first');
                    filament_start_value = x2(1,filament_start_index);
                    filament_end_index = find(gauss2>=(max(gauss2)/2), 1, 'last');
                    filament_end_value = x2(1,filament_end_index);
                    width_filament = filament_end_value - filament_start_value;
                    
                    width_filament_all(length(width_filament_all)+1, 1) = width_filament;
                    
                    
                    
                    % Retrieves the coefficients of the 2 dimensional gaussian
                    a3=f.a3;
                    b3=f.b3;
                    c3=f.c3;
                    
                    % Calculates the Y coordinates of the first Gaussian component 
                    for i=1:length(x2)
                        gauss3(i)=a3*exp(-((x2(i)-b3)/c3)^2);
                    end       
                    
                    plot(x2, gauss3, 'y');
            
                    % Calculates the width of the peak
                    filament_start_index = find(gauss3>=(max(gauss3)/2), 1, 'first');
                    filament_start_value = x2(1,filament_start_index);
                    filament_end_index = find(gauss3>=(max(gauss3)/2), 1, 'last');
                    filament_end_value = x2(1,filament_end_index);
                    width_filament = filament_end_value - filament_start_value;
                    
                    width_filament_all(length(width_filament_all)+1, 1) = width_filament;
                    
                    
                    gauss4 = gauss1 + gauss2 + gauss3;
                    plot(x2, gauss4, 'g');
                    
                    if ~isequal(max(gauss1), 0) 
                    filament_1_max_index = find(gauss1 == max(gauss1));
                    filament_1_max_location = x2(1,filament_1_max_index);
                    end
                    
                    if ~isequal(max(gauss2), 0) 
                    filament_2_max_index = find(gauss2 == max(gauss2));
                    filament_2_max_location = x2(1,filament_2_max_index);
                    end 
                    
                    if ~isequal(max(gauss3), 0) 
                    filament_3_max_index = find(gauss3 == max(gauss3));
                    filament_3_max_location = x2(1,filament_3_max_index);
                    end
                    
                    if exist('filament_1_max_location','var') == 1 && exist('filament_2_max_location','var') == 1                    
                    distance_filaments1 = abs(filament_1_max_location - filament_2_max_location);
                    distance_filaments_all(length(distance_filaments_all)+1, 1) = distance_filaments1;
                    end
                    
                    if exist('filament_2_max_location','var') == 1 && exist('filament_3_max_location','var') == 1    
                    distance_filaments2 = abs(filament_2_max_location - filament_3_max_location);
                    distance_filaments_all(length(distance_filaments_all)+1, 1) = distance_filaments2;
                    end
                    
                    if exist('distance_filaments1','var') && exist('distance_filaments2','var')
                    title(['Distance 1: ', num2str(distance_filaments1),' Distance 2: ', num2str(distance_filaments2)]);
                    clear distance_filaments1 distance_filaments2
                    end
                    
                    if exist('distance_filaments1','var')
                    title(['Peak-to-peak distance: ', num2str(distance_filaments1)]);
                    end
                    
                    if exist('distance_filaments2','var')
                    title(['Peak-to-peak distance: ', num2str(distance_filaments2)]);
                    end
                    
                    clear gauss1 gauss2 gauss3 gauss4 filament_1_max_location filament_2_max_location filament_3_max_location
                    
            end
            
            xlabel('Position (�m)');
            ylabel('Counts');
            
       end
       
end

distFig